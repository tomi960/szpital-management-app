﻿namespace szpital_management
{
    partial class Form1
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.panelSideNav = new System.Windows.Forms.Panel();
            this.buttonVisits = new System.Windows.Forms.Button();
            this.buttonHospital = new System.Windows.Forms.Button();
            this.buttonStaff = new System.Windows.Forms.Button();
            this.buttonPatients = new System.Windows.Forms.Button();
            this.tabControlPatients = new System.Windows.Forms.TabControl();
            this.tabPagePatientsList = new System.Windows.Forms.TabPage();
            this.dataGridPatient = new System.Windows.Forms.DataGridView();
            this.labelPatientListTitle = new System.Windows.Forms.Label();
            this.buttonPatientChoose = new System.Windows.Forms.Button();
            this.buttonPatientDelete = new System.Windows.Forms.Button();
            this.tabPagePatientsAdd = new System.Windows.Forms.TabPage();
            this.buttonPatientAdd = new System.Windows.Forms.Button();
            this.labelPatientAddTitle = new System.Windows.Forms.Label();
            this.labelPatientAddName = new System.Windows.Forms.Label();
            this.textPatientAddName = new System.Windows.Forms.TextBox();
            this.labelPatientAddSurname = new System.Windows.Forms.Label();
            this.textPatientAddSurname = new System.Windows.Forms.TextBox();
            this.labelPatientAddPesel = new System.Windows.Forms.Label();
            this.textPatientAddPesel = new System.Windows.Forms.TextBox();
            this.labelPatientAddPhone = new System.Windows.Forms.Label();
            this.textPatientAddPhone = new System.Windows.Forms.TextBox();
            this.labelPatientAddCity = new System.Windows.Forms.Label();
            this.textPatientAddCity = new System.Windows.Forms.TextBox();
            this.tabControlPatientsVisit = new System.Windows.Forms.TabControl();
            this.tabPagePatientsVisit = new System.Windows.Forms.TabPage();
            this.labelPatientVisitTitle = new System.Windows.Forms.Label();
            this.dataGridPatientVisit = new System.Windows.Forms.DataGridView();
            this.buttonPatientVisitBook = new System.Windows.Forms.Button();
            this.tabControlStaff = new System.Windows.Forms.TabControl();
            this.tabPageStaffList = new System.Windows.Forms.TabPage();
            this.dataGridStaff = new System.Windows.Forms.DataGridView();
            this.labelStaffListTitle = new System.Windows.Forms.Label();
            this.buttonStaffChoose = new System.Windows.Forms.Button();
            this.buttonStaffDelete = new System.Windows.Forms.Button();
            this.tabPageStaffAdd = new System.Windows.Forms.TabPage();
            this.buttonStaffAdd = new System.Windows.Forms.Button();
            this.labelStaffAddTitle = new System.Windows.Forms.Label();
            this.labelStaffAddName = new System.Windows.Forms.Label();
            this.textStaffAddName = new System.Windows.Forms.TextBox();
            this.labelStaffAddSurname = new System.Windows.Forms.Label();
            this.textStaffAddSurname = new System.Windows.Forms.TextBox();
            this.labelStaffAddPesel = new System.Windows.Forms.Label();
            this.textStaffAddPesel = new System.Windows.Forms.TextBox();
            this.labelStaffAddPhone = new System.Windows.Forms.Label();
            this.textStaffAddPhone = new System.Windows.Forms.TextBox();
            this.labelStaffAddCity = new System.Windows.Forms.Label();
            this.textStaffAddCity = new System.Windows.Forms.TextBox();
            this.labelStaffAddPosition = new System.Windows.Forms.Label();
            this.comboStaffAddPosition = new System.Windows.Forms.ComboBox();
            this.tabPageStaffPositionAdd = new System.Windows.Forms.TabPage();
            this.buttonStaffPositionAdd = new System.Windows.Forms.Button();
            this.labelStaffPositionAddTitle = new System.Windows.Forms.Label();
            this.labelStaffPositionAddJob = new System.Windows.Forms.Label();
            this.comboStaffPositionAddJob = new System.Windows.Forms.ComboBox();
            this.labelStaffPositionAddSpecialization = new System.Windows.Forms.Label();
            this.textStaffPositionAddSpecialization = new System.Windows.Forms.TextBox();
            this.tabControlStaffVisit = new System.Windows.Forms.TabControl();
            this.tabPageStaffVisit = new System.Windows.Forms.TabPage();
            this.labelStaffVisitTitle = new System.Windows.Forms.Label();
            this.dataGridStaffVisit = new System.Windows.Forms.DataGridView();
            this.tabControlHospitals = new System.Windows.Forms.TabControl();
            this.tabPageHospitalsList = new System.Windows.Forms.TabPage();
            this.labelHospitalListTitle = new System.Windows.Forms.Label();
            this.dataGridHospital = new System.Windows.Forms.DataGridView();
            this.buttonHospitalChoose = new System.Windows.Forms.Button();
            this.buttonHospitalDelete = new System.Windows.Forms.Button();
            this.tabPageHospitalsAdd = new System.Windows.Forms.TabPage();
            this.buttonHospitalAdd = new System.Windows.Forms.Button();
            this.labelHospitalAddTitle = new System.Windows.Forms.Label();
            this.labelHospitalAddName = new System.Windows.Forms.Label();
            this.textHospitalAddName = new System.Windows.Forms.TextBox();
            this.labelHospitalAddCity = new System.Windows.Forms.Label();
            this.textHospitalAddCity = new System.Windows.Forms.TextBox();
            this.labelHospitalAddAddress = new System.Windows.Forms.Label();
            this.textHospitalAddAddress = new System.Windows.Forms.TextBox();
            this.tabPageHospitalsRoomAdd = new System.Windows.Forms.TabPage();
            this.buttonHospitalsRoomAdd = new System.Windows.Forms.Button();
            this.labelHospitalsRoomAddTitle = new System.Windows.Forms.Label();
            this.labelHospitalsRoomAddNumber = new System.Windows.Forms.Label();
            this.textHospitalsRoomAddNumber = new System.Windows.Forms.TextBox();
            this.labelHospitalsRoomAddFloor = new System.Windows.Forms.Label();
            this.textHospitalsRoomAddFloor = new System.Windows.Forms.TextBox();
            this.labelHospitalsRoomAddHospitalId = new System.Windows.Forms.Label();
            this.comboHospitalsRoomAddHospitalId = new System.Windows.Forms.ComboBox();
            this.tabControlHospitalsVisit = new System.Windows.Forms.TabControl();
            this.tabPageHospitalsVisit = new System.Windows.Forms.TabPage();
            this.labelHospitalVisitTitle = new System.Windows.Forms.Label();
            this.dataGridHospitalVisit = new System.Windows.Forms.DataGridView();
            this.tabControlVisits = new System.Windows.Forms.TabControl();
            this.tabPageVisitsList = new System.Windows.Forms.TabPage();
            this.dataGridVisit = new System.Windows.Forms.DataGridView();
            this.labelVisitListTitle = new System.Windows.Forms.Label();
            this.buttonVisitAssign = new System.Windows.Forms.Button();
            this.buttonVisitDelete = new System.Windows.Forms.Button();
            this.panelScheduleAdd = new System.Windows.Forms.Panel();
            this.labelScheduleStaffName = new System.Windows.Forms.Label();
            this.buttonScheduleSave = new System.Windows.Forms.Button();
            this.comboFridayTo = new System.Windows.Forms.ComboBox();
            this.comboThursdayTo = new System.Windows.Forms.ComboBox();
            this.comboWednesdayTo = new System.Windows.Forms.ComboBox();
            this.comboTuesdayTo = new System.Windows.Forms.ComboBox();
            this.comboMondayTo = new System.Windows.Forms.ComboBox();
            this.comboFridayFrom = new System.Windows.Forms.ComboBox();
            this.comboThursdayFrom = new System.Windows.Forms.ComboBox();
            this.comboWednesdayFrom = new System.Windows.Forms.ComboBox();
            this.comboTuesdayFrom = new System.Windows.Forms.ComboBox();
            this.comboMondayFrom = new System.Windows.Forms.ComboBox();
            this.checkFriday = new System.Windows.Forms.CheckBox();
            this.checkThursday = new System.Windows.Forms.CheckBox();
            this.checkWednesday = new System.Windows.Forms.CheckBox();
            this.checkTuesday = new System.Windows.Forms.CheckBox();
            this.checkMonday = new System.Windows.Forms.CheckBox();
            this.panelVisitAdd = new System.Windows.Forms.Panel();
            this.labelVisitAddTitle = new System.Windows.Forms.Label();
            this.buttonVisitSave = new System.Windows.Forms.Button();
            this.labelVisitAddDate = new System.Windows.Forms.Label();
            this.pickerVisitAddDate = new System.Windows.Forms.DateTimePicker();
            this.labelVisitAddHours = new System.Windows.Forms.Label();
            this.comboVisitAddHours = new System.Windows.Forms.ComboBox();
            this.labelVisitAddHospital = new System.Windows.Forms.Label();
            this.comboVisitAddHospital = new System.Windows.Forms.ComboBox();
            this.labelVisitAddType = new System.Windows.Forms.Label();
            this.comboVisitAddType = new System.Windows.Forms.ComboBox();
            this.panelSideNav.SuspendLayout();
            this.tabControlPatients.SuspendLayout();
            this.tabControlPatientsVisit.SuspendLayout();
            this.tabControlStaff.SuspendLayout();
            this.tabControlStaffVisit.SuspendLayout();
            this.tabControlHospitals.SuspendLayout();
            this.tabControlHospitalsVisit.SuspendLayout();
            this.tabControlVisits.SuspendLayout();
            this.panelScheduleAdd.SuspendLayout();
            this.panelVisitAdd.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSideNav
            // 
            this.panelSideNav.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panelSideNav.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSideNav.Controls.Add(this.buttonVisits);
            this.panelSideNav.Controls.Add(this.buttonHospital);
            this.panelSideNav.Controls.Add(this.buttonStaff);
            this.panelSideNav.Controls.Add(this.buttonPatients);
            this.panelSideNav.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideNav.Location = new System.Drawing.Point(0, 0);
            this.panelSideNav.Name = "panelSideNav";
            this.panelSideNav.Size = new System.Drawing.Size(140, 511);
            this.panelSideNav.TabIndex = 0;
            // 
            // buttonVisits
            // 
            this.buttonVisits.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonVisits.FlatAppearance.BorderColor = System.Drawing.Color.MidnightBlue;
            this.buttonVisits.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonVisits.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonVisits.Location = new System.Drawing.Point(-1, 272);
            this.buttonVisits.Name = "buttonVisits";
            this.buttonVisits.Size = new System.Drawing.Size(140, 58);
            this.buttonVisits.TabIndex = 3;
            this.buttonVisits.Text = "Wizyty";
            this.buttonVisits.UseVisualStyleBackColor = false;
            this.buttonVisits.Click += new System.EventHandler(this.ButtonVisits_Click);
            // 
            // buttonHospital
            // 
            this.buttonHospital.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonHospital.FlatAppearance.BorderColor = System.Drawing.Color.MidnightBlue;
            this.buttonHospital.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHospital.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonHospital.Location = new System.Drawing.Point(-1, 199);
            this.buttonHospital.Name = "buttonHospital";
            this.buttonHospital.Size = new System.Drawing.Size(140, 58);
            this.buttonHospital.TabIndex = 2;
            this.buttonHospital.Text = "Szpital";
            this.buttonHospital.UseVisualStyleBackColor = false;
            this.buttonHospital.Click += new System.EventHandler(this.ButtonHospital_Click);
            // 
            // buttonStaff
            // 
            this.buttonStaff.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonStaff.FlatAppearance.BorderColor = System.Drawing.Color.MidnightBlue;
            this.buttonStaff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonStaff.Location = new System.Drawing.Point(-1, 125);
            this.buttonStaff.Name = "buttonStaff";
            this.buttonStaff.Size = new System.Drawing.Size(140, 58);
            this.buttonStaff.TabIndex = 1;
            this.buttonStaff.Text = "Kadra";
            this.buttonStaff.UseVisualStyleBackColor = false;
            this.buttonStaff.Click += new System.EventHandler(this.ButtonStaff_Click);
            // 
            // buttonPatients
            // 
            this.buttonPatients.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonPatients.FlatAppearance.BorderColor = System.Drawing.Color.MidnightBlue;
            this.buttonPatients.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPatients.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonPatients.Location = new System.Drawing.Point(-1, 52);
            this.buttonPatients.Name = "buttonPatients";
            this.buttonPatients.Size = new System.Drawing.Size(140, 58);
            this.buttonPatients.TabIndex = 0;
            this.buttonPatients.Text = "Pacjenci";
            this.buttonPatients.UseVisualStyleBackColor = false;
            this.buttonPatients.Click += new System.EventHandler(this.ButtonPatients_Click);
            // 
            // tabControlPatients
            // 
            this.tabControlPatients.Controls.Add(this.tabPagePatientsList);
            this.tabControlPatients.Controls.Add(this.tabPagePatientsAdd);
            this.tabControlPatients.Location = new System.Drawing.Point(140, 0);
            this.tabControlPatients.Name = "tabControlPatients";
            this.tabControlPatients.SelectedIndex = 0;
            this.tabControlPatients.Size = new System.Drawing.Size(729, 511);
            this.tabControlPatients.TabIndex = 1;
            // 
            // tabPagePatientsList
            // 
            this.tabPagePatientsList.Controls.Add(this.dataGridPatient);
            this.tabPagePatientsList.Controls.Add(this.labelPatientListTitle);
            this.tabPagePatientsList.Controls.Add(this.buttonPatientChoose);
            this.tabPagePatientsList.Controls.Add(this.buttonPatientDelete);
            this.tabPagePatientsList.Location = new System.Drawing.Point(4, 25);
            this.tabPagePatientsList.Name = "tabPagePatientsList";
            this.tabPagePatientsList.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePatientsList.TabIndex = 0;
            this.tabPagePatientsList.Text = "Lista pacjentów";
            this.tabPagePatientsList.UseVisualStyleBackColor = true;
            // 
            // dataGridPatient
            // 
            this.dataGridPatient.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPatient.Location = new System.Drawing.Point(37, 101);
            this.dataGridPatient.Name = "dataGridPatient";
            this.dataGridPatient.AllowUserToAddRows = false;
            this.dataGridPatient.RowTemplate.Height = 24;
            this.dataGridPatient.Size = new System.Drawing.Size(645, 292);
            this.dataGridPatient.TabIndex = 1;
            this.dataGridPatient.ColumnCount = 5;
            this.dataGridPatient.Columns[0].Name = "Imię";
            this.dataGridPatient.Columns[1].Name = "Nazwisko";
            this.dataGridPatient.Columns[2].Name = "Pesel";
            this.dataGridPatient.Columns[3].Name = "Telefon";
            this.dataGridPatient.Columns[4].Name = "Miasto";
            // 
            // labelPatientListTitle
            // 
            this.labelPatientListTitle.AutoSize = true;
            this.labelPatientListTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPatientListTitle.Location = new System.Drawing.Point(271, 45);
            this.labelPatientListTitle.Name = "labelPatientListTitle";
            this.labelPatientListTitle.Size = new System.Drawing.Size(179, 29);
            this.labelPatientListTitle.TabIndex = 0;
            this.labelPatientListTitle.Text = "Lista pacjentów";
            // 
            // buttonPatientChoose
            // 
            this.buttonPatientChoose.Location = new System.Drawing.Point(254, 399);
            this.buttonPatientChoose.Name = "buttonPatientChoose";
            this.buttonPatientChoose.Size = new System.Drawing.Size(88, 26);
            this.buttonPatientChoose.TabIndex = 11;
            this.buttonPatientChoose.Text = "Wybierz";
            this.buttonPatientChoose.UseVisualStyleBackColor = true;
            this.buttonPatientChoose.Click += new System.EventHandler(this.ButtonPatientChoose_Click);
            // 
            // buttonPatientDelete
            // 
            this.buttonPatientDelete.Location = new System.Drawing.Point(372, 399);
            this.buttonPatientDelete.Name = "buttonPatientDelete";
            this.buttonPatientDelete.Size = new System.Drawing.Size(88, 26);
            this.buttonPatientDelete.TabIndex = 11;
            this.buttonPatientDelete.Text = "Usuń";
            this.buttonPatientDelete.UseVisualStyleBackColor = true;
            this.buttonPatientDelete.Click += new System.EventHandler(this.ButtonPatientDelete_Click);
            // 
            // tabPagePatientsAdd
            // 
            this.tabPagePatientsAdd.Controls.Add(this.buttonPatientAdd);
            this.tabPagePatientsAdd.Controls.Add(this.labelPatientAddCity);
            this.tabPagePatientsAdd.Controls.Add(this.textPatientAddCity);
            this.tabPagePatientsAdd.Controls.Add(this.labelPatientAddPhone);
            this.tabPagePatientsAdd.Controls.Add(this.textPatientAddPhone);
            this.tabPagePatientsAdd.Controls.Add(this.labelPatientAddPesel);
            this.tabPagePatientsAdd.Controls.Add(this.textPatientAddPesel);
            this.tabPagePatientsAdd.Controls.Add(this.labelPatientAddSurname);
            this.tabPagePatientsAdd.Controls.Add(this.textPatientAddSurname);
            this.tabPagePatientsAdd.Controls.Add(this.labelPatientAddName);
            this.tabPagePatientsAdd.Controls.Add(this.textPatientAddName);
            this.tabPagePatientsAdd.Controls.Add(this.labelPatientAddTitle);
            this.tabPagePatientsAdd.Location = new System.Drawing.Point(4, 25);
            this.tabPagePatientsAdd.Name = "tabPagePatientsAdd";
            this.tabPagePatientsAdd.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePatientsAdd.TabIndex = 1;
            this.tabPagePatientsAdd.Text = "Dodaj pacjenta";
            this.tabPagePatientsAdd.UseVisualStyleBackColor = true;
            // 
            // buttonPatientAdd
            // 
            this.buttonPatientAdd.Location = new System.Drawing.Point(320, 315);
            this.buttonPatientAdd.Name = "buttonPatientAdd";
            this.buttonPatientAdd.Size = new System.Drawing.Size(116, 42);
            this.buttonPatientAdd.TabIndex = 11;
            this.buttonPatientAdd.Text = "Dodaj pacjenta";
            this.buttonPatientAdd.UseVisualStyleBackColor = true;
            this.buttonPatientAdd.Click += new System.EventHandler(this.ButtonPatientAdd_Click);
            // 
            // labelPatientAddTitle
            // 
            this.labelPatientAddTitle.AutoSize = true;
            this.labelPatientAddTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPatientAddTitle.Location = new System.Drawing.Point(251, 45);
            this.labelPatientAddTitle.Name = "labelPatientAddTitle";
            this.labelPatientAddTitle.Size = new System.Drawing.Size(232, 29);
            this.labelPatientAddTitle.TabIndex = 0;
            this.labelPatientAddTitle.Text = "Dodawanie pacjenta";
            // 
            // labelPatientAddName
            // 
            this.labelPatientAddName.AutoSize = true;
            this.labelPatientAddName.Location = new System.Drawing.Point(255, 124);
            this.labelPatientAddName.Name = "labelPatientAddName";
            this.labelPatientAddName.Size = new System.Drawing.Size(33, 17);
            this.labelPatientAddName.TabIndex = 2;
            this.labelPatientAddName.Text = "Imię";
            // 
            // textPatientAddName
            // 
            this.textPatientAddName.Location = new System.Drawing.Point(364, 121);
            this.textPatientAddName.Name = "textPatientAddName";
            this.textPatientAddName.Size = new System.Drawing.Size(118, 22);
            this.textPatientAddName.TabIndex = 1;
            // 
            // labelPatientAddSurname
            // 
            this.labelPatientAddSurname.AutoSize = true;
            this.labelPatientAddSurname.Location = new System.Drawing.Point(255, 160);
            this.labelPatientAddSurname.Name = "labelPatientAddSurname";
            this.labelPatientAddSurname.Size = new System.Drawing.Size(67, 17);
            this.labelPatientAddSurname.TabIndex = 4;
            this.labelPatientAddSurname.Text = "Nazwisko";
            // 
            // textPatientAddSurname
            // 
            this.textPatientAddSurname.Location = new System.Drawing.Point(364, 157);
            this.textPatientAddSurname.Name = "textPatientAddSurname";
            this.textPatientAddSurname.Size = new System.Drawing.Size(118, 22);
            this.textPatientAddSurname.TabIndex = 3;
            // 
            // labelPatientAddPesel
            // 
            this.labelPatientAddPesel.AutoSize = true;
            this.labelPatientAddPesel.Location = new System.Drawing.Point(255, 198);
            this.labelPatientAddPesel.Name = "labelPatientAddPesel";
            this.labelPatientAddPesel.Size = new System.Drawing.Size(52, 17);
            this.labelPatientAddPesel.TabIndex = 6;
            this.labelPatientAddPesel.Text = "PESEL";
            // 
            // textPatientAddPesel
            // 
            this.textPatientAddPesel.Location = new System.Drawing.Point(364, 195);
            this.textPatientAddPesel.Name = "textPatientAddPesel";
            this.textPatientAddPesel.Size = new System.Drawing.Size(118, 22);
            this.textPatientAddPesel.TabIndex = 5;
            // 
            // labelPatientAddPhone
            // 
            this.labelPatientAddPhone.AutoSize = true;
            this.labelPatientAddPhone.Location = new System.Drawing.Point(255, 234);
            this.labelPatientAddPhone.Name = "labelPatientAddPhone";
            this.labelPatientAddPhone.Size = new System.Drawing.Size(56, 17);
            this.labelPatientAddPhone.TabIndex = 8;
            this.labelPatientAddPhone.Text = "Telefon";
            // 
            // textPatientAddPhone
            // 
            this.textPatientAddPhone.Location = new System.Drawing.Point(364, 231);
            this.textPatientAddPhone.Name = "textPatientAddPhone";
            this.textPatientAddPhone.Size = new System.Drawing.Size(118, 22);
            this.textPatientAddPhone.TabIndex = 7;
            // 
            // labelPatientAddCity
            // 
            this.labelPatientAddCity.AutoSize = true;
            this.labelPatientAddCity.Location = new System.Drawing.Point(255, 271);
            this.labelPatientAddCity.Name = "labelPatientAddCity";
            this.labelPatientAddCity.Size = new System.Drawing.Size(49, 17);
            this.labelPatientAddCity.TabIndex = 10;
            this.labelPatientAddCity.Text = "Miasto";
            // 
            // textPatientAddCity
            // 
            this.textPatientAddCity.Location = new System.Drawing.Point(364, 268);
            this.textPatientAddCity.Name = "textPatientAddCity";
            this.textPatientAddCity.Size = new System.Drawing.Size(118, 22);
            this.textPatientAddCity.TabIndex = 9;
            // 
            // tabControlPatientsVisit
            // 
            this.tabControlPatientsVisit.Controls.Add(this.tabPagePatientsVisit);
            this.tabControlPatientsVisit.Location = new System.Drawing.Point(140, 0);
            this.tabControlPatientsVisit.Name = "tabControlPatientsVisit";
            this.tabControlPatientsVisit.SelectedIndex = 0;
            this.tabControlPatientsVisit.Size = new System.Drawing.Size(729, 511);
            this.tabControlPatientsVisit.TabIndex = 1;
            this.tabControlPatientsVisit.Visible = false;
            // 
            // tabPagePatientsVisit
            // 
            this.tabPagePatientsVisit.Controls.Add(this.labelPatientVisitTitle);
            this.tabPagePatientsVisit.Controls.Add(this.dataGridPatientVisit);
            this.tabPagePatientsVisit.Controls.Add(this.buttonPatientVisitBook);
            this.tabPagePatientsVisit.Location = new System.Drawing.Point(4, 25);
            this.tabPagePatientsVisit.Name = "tabPagePatientsVisit";
            this.tabPagePatientsVisit.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePatientsVisit.TabIndex = 0;
            this.tabPagePatientsVisit.Text = "Pacjent";
            this.tabPagePatientsVisit.UseVisualStyleBackColor = true;
            // 
            // labelPatientVisitTitle
            // 
            this.labelPatientVisitTitle.AutoSize = true;
            this.labelPatientVisitTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPatientVisitTitle.Location = new System.Drawing.Point(243, 45);
            this.labelPatientVisitTitle.Name = "labelPatientVisitTitle";
            this.labelPatientVisitTitle.Size = new System.Drawing.Size(179, 29);
            this.labelPatientVisitTitle.TabIndex = 0;
            this.labelPatientVisitTitle.Text = "";
            // 
            // dataGridPatientVisit
            // 
            this.dataGridPatientVisit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPatientVisit.Location = new System.Drawing.Point(37, 101);
            this.dataGridPatientVisit.Name = "dataGridPatientVisit";
            this.dataGridPatientVisit.AllowUserToAddRows = false;
            this.dataGridPatientVisit.RowTemplate.Height = 24;
            this.dataGridPatientVisit.Size = new System.Drawing.Size(645, 292);
            this.dataGridPatientVisit.TabIndex = 1;
            this.dataGridPatientVisit.ColumnCount = 5;
            this.dataGridPatientVisit.Columns[0].Name = "Termin";
            this.dataGridPatientVisit.Columns[1].Name = "Czas trwania";
            this.dataGridPatientVisit.Columns[2].Name = "Lekarz";
            this.dataGridPatientVisit.Columns[3].Name = "Sala";
            this.dataGridPatientVisit.Columns[4].Name = "Typ wizyty";
            // 
            // buttonPatientVisitBook
            // 
            this.buttonPatientVisitBook.Location = new System.Drawing.Point(314, 399);
            this.buttonPatientVisitBook.Name = "buttonPatientVisitBook";
            this.buttonPatientVisitBook.Size = new System.Drawing.Size(88, 26);
            this.buttonPatientVisitBook.TabIndex = 11;
            this.buttonPatientVisitBook.Text = "Rezerwuj";
            this.buttonPatientVisitBook.UseVisualStyleBackColor = true;
            this.buttonPatientVisitBook.Click += new System.EventHandler(this.ButtonPatientVisitBook_Click);
            // 
            // tabControlStaff
            // 
            this.tabControlStaff.Controls.Add(this.tabPageStaffList);
            this.tabControlStaff.Controls.Add(this.tabPageStaffAdd);
            this.tabControlStaff.Controls.Add(this.tabPageStaffPositionAdd);
            this.tabControlStaff.Location = new System.Drawing.Point(140, 0);
            this.tabControlStaff.Name = "tabControlStaff";
            this.tabControlStaff.SelectedIndex = 0;
            this.tabControlStaff.Size = new System.Drawing.Size(729, 511);
            this.tabControlStaff.TabIndex = 1;
            this.tabControlStaff.Visible = false;
            // 
            // tabPageStaffList
            // 
            this.tabPageStaffList.Controls.Add(this.dataGridStaff);
            this.tabPageStaffList.Controls.Add(this.labelStaffListTitle);
            this.tabPageStaffList.Controls.Add(this.buttonStaffChoose);
            this.tabPageStaffList.Controls.Add(this.buttonStaffDelete);
            this.tabPageStaffList.Location = new System.Drawing.Point(4, 25);
            this.tabPageStaffList.Name = "tabPageStaffList";
            this.tabPageStaffList.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStaffList.TabIndex = 0;
            this.tabPageStaffList.Text = "Lista personelu";
            this.tabPageStaffList.UseVisualStyleBackColor = true;
            // 
            // dataGridStaff
            // 
            this.dataGridStaff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridStaff.Location = new System.Drawing.Point(37, 101);
            this.dataGridStaff.Name = "dataGridStaff";
            this.dataGridStaff.AllowUserToAddRows = false;
            this.dataGridStaff.RowTemplate.Height = 24;
            this.dataGridStaff.Size = new System.Drawing.Size(645, 292);
            this.dataGridStaff.TabIndex = 1;
            this.dataGridStaff.ColumnCount = 6;
            this.dataGridStaff.Columns[0].Name = "Imię";
            this.dataGridStaff.Columns[1].Name = "Nazwisko";
            this.dataGridStaff.Columns[2].Name = "Pesel";
            this.dataGridStaff.Columns[3].Name = "Telefon";
            this.dataGridStaff.Columns[4].Name = "Miasto";
            this.dataGridStaff.Columns[5].Name = "Stanowisko";
            // 
            // labelStaffListTitle
            // 
            this.labelStaffListTitle.AutoSize = true;
            this.labelStaffListTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStaffListTitle.Location = new System.Drawing.Point(271, 45);
            this.labelStaffListTitle.Name = "labelStaffListTitle";
            this.labelStaffListTitle.Size = new System.Drawing.Size(179, 29);
            this.labelStaffListTitle.TabIndex = 0;
            this.labelStaffListTitle.Text = "Lista personelu";
            // 
            // buttonStaffChoose
            // 
            this.buttonStaffChoose.Location = new System.Drawing.Point(254, 399);
            this.buttonStaffChoose.Name = "buttonStaffChoose";
            this.buttonStaffChoose.Size = new System.Drawing.Size(88, 26);
            this.buttonStaffChoose.TabIndex = 11;
            this.buttonStaffChoose.Text = "Wybierz";
            this.buttonStaffChoose.UseVisualStyleBackColor = true;
            this.buttonStaffChoose.Click += new System.EventHandler(this.ButtonStaffChoose_Click);
            // 
            // buttonStaffDelete
            // 
            this.buttonStaffDelete.Location = new System.Drawing.Point(372, 399);
            this.buttonStaffDelete.Name = "buttonStaffDelete";
            this.buttonStaffDelete.Size = new System.Drawing.Size(88, 26);
            this.buttonStaffDelete.TabIndex = 11;
            this.buttonStaffDelete.Text = "Usuń";
            this.buttonStaffDelete.UseVisualStyleBackColor = true;
            this.buttonStaffDelete.Click += new System.EventHandler(this.ButtonStaffDelete_Click);
            // 
            // tabPageStaffAdd
            // 
            this.tabPageStaffAdd.Controls.Add(this.buttonStaffAdd);
            this.tabPageStaffAdd.Controls.Add(this.labelStaffAddCity);
            this.tabPageStaffAdd.Controls.Add(this.textStaffAddCity);
            this.tabPageStaffAdd.Controls.Add(this.labelStaffAddPhone);
            this.tabPageStaffAdd.Controls.Add(this.textStaffAddPhone);
            this.tabPageStaffAdd.Controls.Add(this.labelStaffAddPesel);
            this.tabPageStaffAdd.Controls.Add(this.textStaffAddPesel);
            this.tabPageStaffAdd.Controls.Add(this.labelStaffAddSurname);
            this.tabPageStaffAdd.Controls.Add(this.textStaffAddSurname);
            this.tabPageStaffAdd.Controls.Add(this.labelStaffAddName);
            this.tabPageStaffAdd.Controls.Add(this.textStaffAddName);
            this.tabPageStaffAdd.Controls.Add(this.labelStaffAddTitle);
            this.tabPageStaffAdd.Controls.Add(this.labelStaffAddPosition);
            this.tabPageStaffAdd.Controls.Add(this.comboStaffAddPosition);
            this.tabPageStaffAdd.Location = new System.Drawing.Point(4, 25);
            this.tabPageStaffAdd.Name = "tabPageStaffAdd";
            this.tabPageStaffAdd.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStaffAdd.TabIndex = 1;
            this.tabPageStaffAdd.Text = "Dodaj kadrę";
            this.tabPageStaffAdd.UseVisualStyleBackColor = true;
            // 
            // buttonStaffAdd
            // 
            this.buttonStaffAdd.Location = new System.Drawing.Point(320, 351);
            this.buttonStaffAdd.Name = "buttonStaffAdd";
            this.buttonStaffAdd.Size = new System.Drawing.Size(116, 42);
            this.buttonStaffAdd.TabIndex = 13;
            this.buttonStaffAdd.Text = "Dodaj kadrę";
            this.buttonStaffAdd.UseVisualStyleBackColor = true;
            this.buttonStaffAdd.Click += new System.EventHandler(this.ButtonStaffAdd_Click);
            // 
            // labelStaffAddTitle
            // 
            this.labelStaffAddTitle.AutoSize = true;
            this.labelStaffAddTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStaffAddTitle.Location = new System.Drawing.Point(266, 45);
            this.labelStaffAddTitle.Name = "labelStaffAddTitle";
            this.labelStaffAddTitle.Size = new System.Drawing.Size(232, 29);
            this.labelStaffAddTitle.TabIndex = 0;
            this.labelStaffAddTitle.Text = "Dodawanie kadry";
            // 
            // labelStaffAddName
            // 
            this.labelStaffAddName.AutoSize = true;
            this.labelStaffAddName.Location = new System.Drawing.Point(255, 124);
            this.labelStaffAddName.Name = "labelStaffAddName";
            this.labelStaffAddName.Size = new System.Drawing.Size(33, 17);
            this.labelStaffAddName.TabIndex = 2;
            this.labelStaffAddName.Text = "Imię";
            // 
            // textStaffAddName
            // 
            this.textStaffAddName.Location = new System.Drawing.Point(364, 121);
            this.textStaffAddName.Name = "textStaffAddName";
            this.textStaffAddName.Size = new System.Drawing.Size(118, 22);
            this.textStaffAddName.TabIndex = 1;
            // 
            // labelStaffAddSurname
            // 
            this.labelStaffAddSurname.AutoSize = true;
            this.labelStaffAddSurname.Location = new System.Drawing.Point(255, 160);
            this.labelStaffAddSurname.Name = "labelStaffAddSurname";
            this.labelStaffAddSurname.Size = new System.Drawing.Size(67, 17);
            this.labelStaffAddSurname.TabIndex = 4;
            this.labelStaffAddSurname.Text = "Nazwisko";
            // 
            // textStaffAddSurname
            // 
            this.textStaffAddSurname.Location = new System.Drawing.Point(364, 157);
            this.textStaffAddSurname.Name = "textStaffAddSurname";
            this.textStaffAddSurname.Size = new System.Drawing.Size(118, 22);
            this.textStaffAddSurname.TabIndex = 3;
            // 
            // labelStaffAddPesel
            // 
            this.labelStaffAddPesel.AutoSize = true;
            this.labelStaffAddPesel.Location = new System.Drawing.Point(255, 198);
            this.labelStaffAddPesel.Name = "labelStaffAddPesel";
            this.labelStaffAddPesel.Size = new System.Drawing.Size(52, 17);
            this.labelStaffAddPesel.TabIndex = 6;
            this.labelStaffAddPesel.Text = "PESEL";
            // 
            // textStaffAddPesel
            // 
            this.textStaffAddPesel.Location = new System.Drawing.Point(364, 195);
            this.textStaffAddPesel.Name = "textStaffAddPesel";
            this.textStaffAddPesel.Size = new System.Drawing.Size(118, 22);
            this.textStaffAddPesel.TabIndex = 5;
            // 
            // labelStaffAddPhone
            // 
            this.labelStaffAddPhone.AutoSize = true;
            this.labelStaffAddPhone.Location = new System.Drawing.Point(255, 234);
            this.labelStaffAddPhone.Name = "labelStaffAddPhone";
            this.labelStaffAddPhone.Size = new System.Drawing.Size(56, 17);
            this.labelStaffAddPhone.TabIndex = 8;
            this.labelStaffAddPhone.Text = "Telefon";
            // 
            // textStaffAddPhone
            // 
            this.textStaffAddPhone.Location = new System.Drawing.Point(364, 231);
            this.textStaffAddPhone.Name = "textStaffAddPhone";
            this.textStaffAddPhone.Size = new System.Drawing.Size(118, 22);
            this.textStaffAddPhone.TabIndex = 7;
            // 
            // labelStaffAddCity
            // 
            this.labelStaffAddCity.AutoSize = true;
            this.labelStaffAddCity.Location = new System.Drawing.Point(255, 271);
            this.labelStaffAddCity.Name = "labelStaffAddCity";
            this.labelStaffAddCity.Size = new System.Drawing.Size(49, 17);
            this.labelStaffAddCity.TabIndex = 10;
            this.labelStaffAddCity.Text = "Miasto";
            // 
            // textStaffAddCity
            // 
            this.textStaffAddCity.Location = new System.Drawing.Point(364, 268);
            this.textStaffAddCity.Name = "textStaffAddCity";
            this.textStaffAddCity.Size = new System.Drawing.Size(118, 22);
            this.textStaffAddCity.TabIndex = 9;
            // 
            // labelStaffAddPosition
            // 
            this.labelStaffAddPosition.AutoSize = true;
            this.labelStaffAddPosition.Location = new System.Drawing.Point(255, 304);
            this.labelStaffAddPosition.Name = "labelStaffAddPosition";
            this.labelStaffAddPosition.Size = new System.Drawing.Size(49, 17);
            this.labelStaffAddPosition.TabIndex = 12;
            this.labelStaffAddPosition.Text = "Stanowisko";
            // 
            // comboStaffAddPosition
            // 
            this.comboStaffAddPosition.Location = new System.Drawing.Point(364, 304);
            this.comboStaffAddPosition.Name = "comboStaffAddPosition";
            this.comboStaffAddPosition.Size = new System.Drawing.Size(118, 22);
            this.comboStaffAddPosition.TabIndex = 11;
            // 
            // tabPageStaffPositionAdd
            // 
            this.tabPageStaffPositionAdd.Controls.Add(this.buttonStaffPositionAdd);
            this.tabPageStaffPositionAdd.Controls.Add(this.labelStaffPositionAddTitle);
            this.tabPageStaffPositionAdd.Controls.Add(this.labelStaffPositionAddJob);
            this.tabPageStaffPositionAdd.Controls.Add(this.comboStaffPositionAddJob);
            this.tabPageStaffPositionAdd.Controls.Add(this.labelStaffPositionAddSpecialization);
            this.tabPageStaffPositionAdd.Controls.Add(this.textStaffPositionAddSpecialization);
            this.tabPageStaffPositionAdd.Location = new System.Drawing.Point(4, 25);
            this.tabPageStaffPositionAdd.Name = "tabPageStaffPositionAdd";
            this.tabPageStaffPositionAdd.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStaffPositionAdd.TabIndex = 1;
            this.tabPageStaffPositionAdd.Text = "Dodaj stanowisko";
            this.tabPageStaffPositionAdd.UseVisualStyleBackColor = true;
            // 
            // buttonStaffPositionAdd
            // 
            this.buttonStaffPositionAdd.Location = new System.Drawing.Point(310, 213);
            this.buttonStaffPositionAdd.Name = "buttonStaffPositionAdd";
            this.buttonStaffPositionAdd.Size = new System.Drawing.Size(136, 42);
            this.buttonStaffPositionAdd.TabIndex = 11;
            this.buttonStaffPositionAdd.Text = "Dodaj stanowisko";
            this.buttonStaffPositionAdd.UseVisualStyleBackColor = true;
            this.buttonStaffPositionAdd.Click += new System.EventHandler(this.ButtonStaffPositionAdd_Click);
            // 
            // labelStaffPositionAddTitle
            // 
            this.labelStaffPositionAddTitle.AutoSize = true;
            this.labelStaffPositionAddTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStaffPositionAddTitle.Location = new System.Drawing.Point(241, 45);
            this.labelStaffPositionAddTitle.Name = "labelStaffPositionAddTitle";
            this.labelStaffPositionAddTitle.Size = new System.Drawing.Size(232, 29);
            this.labelStaffPositionAddTitle.TabIndex = 0;
            this.labelStaffPositionAddTitle.Text = "Dodawanie stanowiska";
            // 
            // labelStaffPositionAddJob
            // 
            this.labelStaffPositionAddJob.AutoSize = true;
            this.labelStaffPositionAddJob.Location = new System.Drawing.Point(255, 124);
            this.labelStaffPositionAddJob.Name = "labelStaffPositionAddJob";
            this.labelStaffPositionAddJob.Size = new System.Drawing.Size(49, 17);
            this.labelStaffPositionAddJob.TabIndex = 5;
            this.labelStaffPositionAddJob.Text = "Zawód";
            // 
            // comboStaffPositionAddJob
            // 
            this.comboStaffPositionAddJob.Location = new System.Drawing.Point(364, 121);
            this.comboStaffPositionAddJob.Name = "textHospitalsRoomAddNumber";
            this.comboStaffPositionAddJob.Size = new System.Drawing.Size(118, 22);
            this.comboStaffPositionAddJob.TabIndex = 6;
            this.comboStaffPositionAddJob.Items.AddRange(new string[3] { "Lekarz", "Pielęgniarka", "Chirurg" });
            // 
            // labelStaffPositionAddSpecialization
            // 
            this.labelStaffPositionAddSpecialization.AutoSize = true;
            this.labelStaffPositionAddSpecialization.Location = new System.Drawing.Point(255, 160);
            this.labelStaffPositionAddSpecialization.Name = "labelStaffPositionAddSpecialization";
            this.labelStaffPositionAddSpecialization.Size = new System.Drawing.Size(56, 17);
            this.labelStaffPositionAddSpecialization.TabIndex = 7;
            this.labelStaffPositionAddSpecialization.Text = "Stanowisko";
            // 
            // textStaffPositionAddSpecialization
            // 
            this.textStaffPositionAddSpecialization.Location = new System.Drawing.Point(364, 157);
            this.textStaffPositionAddSpecialization.Name = "textStaffPositionAddSpecialization";
            this.textStaffPositionAddSpecialization.Size = new System.Drawing.Size(118, 22);
            this.textStaffPositionAddSpecialization.TabIndex = 8;
            // 
            // tabControlStaffVisit
            // 
            this.tabControlStaffVisit.Controls.Add(this.tabPageStaffVisit);
            this.tabControlStaffVisit.Location = new System.Drawing.Point(140, 0);
            this.tabControlStaffVisit.Name = "tabControlStaffVisit";
            this.tabControlStaffVisit.SelectedIndex = 0;
            this.tabControlStaffVisit.Size = new System.Drawing.Size(729, 511);
            this.tabControlStaffVisit.TabIndex = 1;
            this.tabControlStaffVisit.Visible = false;
            // 
            // tabPageStaffVisit
            // 
            this.tabPageStaffVisit.Controls.Add(this.labelStaffVisitTitle);
            this.tabPageStaffVisit.Controls.Add(this.dataGridStaffVisit);
            this.tabPageStaffVisit.Location = new System.Drawing.Point(4, 25);
            this.tabPageStaffVisit.Name = "tabPageStaffVisit";
            this.tabPageStaffVisit.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStaffVisit.TabIndex = 0;
            this.tabPageStaffVisit.Text = "Kadra";
            this.tabPageStaffVisit.UseVisualStyleBackColor = true;
            // 
            // labelStaffVisitTitle
            // 
            this.labelStaffVisitTitle.AutoSize = true;
            this.labelStaffVisitTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStaffVisitTitle.Location = new System.Drawing.Point(203, 45);
            this.labelStaffVisitTitle.Name = "labelStaffVisitTitle";
            this.labelStaffVisitTitle.Size = new System.Drawing.Size(179, 29);
            this.labelStaffVisitTitle.TabIndex = 0;
            this.labelStaffVisitTitle.Text = "";
            // 
            // dataGridStaffVisit
            // 
            this.dataGridStaffVisit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridStaffVisit.Location = new System.Drawing.Point(37, 101);
            this.dataGridStaffVisit.Name = "dataGridStaffVisit";
            this.dataGridStaffVisit.AllowUserToAddRows = false;
            this.dataGridStaffVisit.RowTemplate.Height = 24;
            this.dataGridStaffVisit.Size = new System.Drawing.Size(645, 292);
            this.dataGridStaffVisit.TabIndex = 1;
            this.dataGridStaffVisit.ColumnCount = 5;
            this.dataGridStaffVisit.Columns[0].Name = "Termin";
            this.dataGridStaffVisit.Columns[1].Name = "Czas trwania";
            this.dataGridStaffVisit.Columns[2].Name = "Sala";
            this.dataGridStaffVisit.Columns[3].Name = "Pacjent";
            this.dataGridStaffVisit.Columns[4].Name = "Typ wizyty";
            // 
            // tabControlHospitals
            // 
            this.tabControlHospitals.Controls.Add(this.tabPageHospitalsList);
            this.tabControlHospitals.Controls.Add(this.tabPageHospitalsAdd);
            this.tabControlHospitals.Controls.Add(this.tabPageHospitalsRoomAdd);
            this.tabControlHospitals.Location = new System.Drawing.Point(140, 0);
            this.tabControlHospitals.Name = "tabControlHospitals";
            this.tabControlHospitals.SelectedIndex = 0;
            this.tabControlHospitals.Size = new System.Drawing.Size(729, 511);
            this.tabControlHospitals.TabIndex = 1;
            this.tabControlHospitals.Visible = false;
            // 
            // tabPageHospitalsList
            // 
            this.tabPageHospitalsList.Controls.Add(this.labelHospitalListTitle);
            this.tabPageHospitalsList.Controls.Add(this.dataGridHospital);
            this.tabPageHospitalsList.Controls.Add(this.buttonHospitalChoose);
            this.tabPageHospitalsList.Controls.Add(this.buttonHospitalDelete);
            this.tabPageHospitalsList.Location = new System.Drawing.Point(4, 25);
            this.tabPageHospitalsList.Name = "tabPageHospitalsList";
            this.tabPageHospitalsList.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHospitalsList.TabIndex = 0;
            this.tabPageHospitalsList.Text = "Lista szpitali";
            this.tabPageHospitalsList.UseVisualStyleBackColor = true;
            // 
            // labelHospitalListTitle
            // 
            this.labelHospitalListTitle.AutoSize = true;
            this.labelHospitalListTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHospitalListTitle.Location = new System.Drawing.Point(271, 45);
            this.labelHospitalListTitle.Name = "labelHospitalListTitle";
            this.labelHospitalListTitle.Size = new System.Drawing.Size(143, 29);
            this.labelHospitalListTitle.TabIndex = 0;
            this.labelHospitalListTitle.Text = "Lista szpitali";
            // 
            // dataGridHospital
            // 
            this.dataGridHospital.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridHospital.Location = new System.Drawing.Point(118, 101);
            this.dataGridHospital.Name = "dataGridHospital";
            this.dataGridHospital.AllowUserToAddRows = false;
            this.dataGridHospital.RowTemplate.Height = 24;
            this.dataGridHospital.Size = new System.Drawing.Size(456, 292);
            this.dataGridHospital.TabIndex = 1;
            this.dataGridHospital.ColumnCount = 3;
            this.dataGridHospital.Columns[0].Name = "Nazwa";
            this.dataGridHospital.Columns[1].Name = "Miasto";
            this.dataGridHospital.Columns[2].Name = "Adres";
            // 
            // buttonHospitalChoose
            // 
            this.buttonHospitalChoose.Location = new System.Drawing.Point(254, 399);
            this.buttonHospitalChoose.Name = "buttonHospitalChoose";
            this.buttonHospitalChoose.Size = new System.Drawing.Size(88, 26);
            this.buttonHospitalChoose.TabIndex = 11;
            this.buttonHospitalChoose.Text = "Wybierz";
            this.buttonHospitalChoose.UseVisualStyleBackColor = true;
            this.buttonHospitalChoose.Click += new System.EventHandler(this.ButtonHospitalChoose_Click);
            // 
            // buttonHospitalDelete
            // 
            this.buttonHospitalDelete.Location = new System.Drawing.Point(372, 399);
            this.buttonHospitalDelete.Name = "buttonHospitalDelete";
            this.buttonHospitalDelete.Size = new System.Drawing.Size(88, 26);
            this.buttonHospitalDelete.TabIndex = 11;
            this.buttonHospitalDelete.Text = "Usuń";
            this.buttonHospitalDelete.UseVisualStyleBackColor = true;
            this.buttonHospitalDelete.Click += new System.EventHandler(this.ButtonHospitalDelete_Click);
            // 
            // tabPageHospitalsAdd
            // 
            this.tabPageHospitalsAdd.Controls.Add(this.buttonHospitalAdd);
            this.tabPageHospitalsAdd.Controls.Add(this.labelHospitalAddTitle);
            this.tabPageHospitalsAdd.Controls.Add(this.labelHospitalAddName);
            this.tabPageHospitalsAdd.Controls.Add(this.textHospitalAddName);
            this.tabPageHospitalsAdd.Controls.Add(this.labelHospitalAddCity);
            this.tabPageHospitalsAdd.Controls.Add(this.textHospitalAddCity);
            this.tabPageHospitalsAdd.Controls.Add(this.labelHospitalAddAddress);
            this.tabPageHospitalsAdd.Controls.Add(this.textHospitalAddAddress);
            this.tabPageHospitalsAdd.Location = new System.Drawing.Point(4, 25);
            this.tabPageHospitalsAdd.Name = "tabPageHospitalsAdd";
            this.tabPageHospitalsAdd.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHospitalsAdd.TabIndex = 1;
            this.tabPageHospitalsAdd.Text = "Dodaj szpital";
            this.tabPageHospitalsAdd.UseVisualStyleBackColor = true;
            // 
            // buttonHospitalAdd
            // 
            this.buttonHospitalAdd.Location = new System.Drawing.Point(320, 245);
            this.buttonHospitalAdd.Name = "buttonHospitalAdd";
            this.buttonHospitalAdd.Size = new System.Drawing.Size(116, 42);
            this.buttonHospitalAdd.TabIndex = 11;
            this.buttonHospitalAdd.Text = "Dodaj szpital";
            this.buttonHospitalAdd.UseVisualStyleBackColor = true;
            this.buttonHospitalAdd.Click += new System.EventHandler(this.ButtonHospitalAdd_Click);
            // 
            // labelHospitalAddTitle
            // 
            this.labelHospitalAddTitle.AutoSize = true;
            this.labelHospitalAddTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHospitalAddTitle.Location = new System.Drawing.Point(256, 45);
            this.labelHospitalAddTitle.Name = "labelHospitalAddTitle";
            this.labelHospitalAddTitle.Size = new System.Drawing.Size(232, 29);
            this.labelHospitalAddTitle.TabIndex = 0;
            this.labelHospitalAddTitle.Text = "Dodawanie szpitala";
            // 
            // labelHospitalAddName
            // 
            this.labelHospitalAddName.AutoSize = true;
            this.labelHospitalAddName.Location = new System.Drawing.Point(255, 124);
            this.labelHospitalAddName.Name = "labelHospitalAddName";
            this.labelHospitalAddName.Size = new System.Drawing.Size(49, 17);
            this.labelHospitalAddName.TabIndex = 5;
            this.labelHospitalAddName.Text = "Nazwa";
            // 
            // textHospitalAddName
            // 
            this.textHospitalAddName.Location = new System.Drawing.Point(364, 121);
            this.textHospitalAddName.Name = "textHospitalAddName";
            this.textHospitalAddName.Size = new System.Drawing.Size(118, 22);
            this.textHospitalAddName.TabIndex = 6;
            // 
            // labelHospitalAddCity
            // 
            this.labelHospitalAddCity.AutoSize = true;
            this.labelHospitalAddCity.Location = new System.Drawing.Point(255, 160);
            this.labelHospitalAddCity.Name = "labelHospitalAddCity";
            this.labelHospitalAddCity.Size = new System.Drawing.Size(56, 17);
            this.labelHospitalAddCity.TabIndex = 7;
            this.labelHospitalAddCity.Text = "Miasto";
            // 
            // textHospitalAddCity
            // 
            this.textHospitalAddCity.Location = new System.Drawing.Point(364, 157);
            this.textHospitalAddCity.Name = "textHospitalAddCity";
            this.textHospitalAddCity.Size = new System.Drawing.Size(118, 22);
            this.textHospitalAddCity.TabIndex = 8;
            // 
            // labelHospitalAddAddress
            // 
            this.labelHospitalAddAddress.AutoSize = true;
            this.labelHospitalAddAddress.Location = new System.Drawing.Point(255, 198);
            this.labelHospitalAddAddress.Name = "labelHospitalAddAddress";
            this.labelHospitalAddAddress.Size = new System.Drawing.Size(52, 17);
            this.labelHospitalAddAddress.TabIndex = 9;
            this.labelHospitalAddAddress.Text = "Adres";
            // 
            // textHospitalAddAddress
            // 
            this.textHospitalAddAddress.Location = new System.Drawing.Point(364, 195);
            this.textHospitalAddAddress.Name = "textHospitalAddAddress";
            this.textHospitalAddAddress.Size = new System.Drawing.Size(118, 22);
            this.textHospitalAddAddress.TabIndex = 10;
            // 
            // tabPageHospitalsRoomAdd
            // 
            this.tabPageHospitalsRoomAdd.Controls.Add(this.buttonHospitalsRoomAdd);
            this.tabPageHospitalsRoomAdd.Controls.Add(this.labelHospitalsRoomAddTitle);
            this.tabPageHospitalsRoomAdd.Controls.Add(this.labelHospitalsRoomAddNumber);
            this.tabPageHospitalsRoomAdd.Controls.Add(this.textHospitalsRoomAddNumber);
            this.tabPageHospitalsRoomAdd.Controls.Add(this.labelHospitalsRoomAddFloor);
            this.tabPageHospitalsRoomAdd.Controls.Add(this.textHospitalsRoomAddFloor);
            this.tabPageHospitalsRoomAdd.Controls.Add(this.labelHospitalsRoomAddHospitalId);
            this.tabPageHospitalsRoomAdd.Controls.Add(this.comboHospitalsRoomAddHospitalId);
            this.tabPageHospitalsRoomAdd.Location = new System.Drawing.Point(4, 25);
            this.tabPageHospitalsRoomAdd.Name = "tabPageHospitalsRoomAdd";
            this.tabPageHospitalsRoomAdd.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHospitalsRoomAdd.TabIndex = 1;
            this.tabPageHospitalsRoomAdd.Text = "Dodaj salę";
            this.tabPageHospitalsRoomAdd.UseVisualStyleBackColor = true;
            // 
            // buttonHospitalsRoomAdd
            // 
            this.buttonHospitalsRoomAdd.Location = new System.Drawing.Point(320, 245);
            this.buttonHospitalsRoomAdd.Name = "buttonHospitalsRoomAdd";
            this.buttonHospitalsRoomAdd.Size = new System.Drawing.Size(116, 42);
            this.buttonHospitalsRoomAdd.TabIndex = 11;
            this.buttonHospitalsRoomAdd.Text = "Dodaj salę";
            this.buttonHospitalsRoomAdd.UseVisualStyleBackColor = true;
            this.buttonHospitalsRoomAdd.Click += new System.EventHandler(this.ButtonHospitalsRoomAdd_Click);
            // 
            // labelHospitalsRoomAddTitle
            // 
            this.labelHospitalsRoomAddTitle.AutoSize = true;
            this.labelHospitalsRoomAddTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHospitalsRoomAddTitle.Location = new System.Drawing.Point(277, 45);
            this.labelHospitalsRoomAddTitle.Name = "labelHospitalsRoomAddTitle";
            this.labelHospitalsRoomAddTitle.Size = new System.Drawing.Size(232, 29);
            this.labelHospitalsRoomAddTitle.TabIndex = 0;
            this.labelHospitalsRoomAddTitle.Text = "Dodawanie sali";
            // 
            // labelHospitalsRoomAddNumber
            // 
            this.labelHospitalsRoomAddNumber.AutoSize = true;
            this.labelHospitalsRoomAddNumber.Location = new System.Drawing.Point(255, 124);
            this.labelHospitalsRoomAddNumber.Name = "labelHospitalsRoomAddNumber";
            this.labelHospitalsRoomAddNumber.Size = new System.Drawing.Size(49, 17);
            this.labelHospitalsRoomAddNumber.TabIndex = 5;
            this.labelHospitalsRoomAddNumber.Text = "Numer";
            // 
            // textHospitalsRoomAddNumber
            // 
            this.textHospitalsRoomAddNumber.Location = new System.Drawing.Point(364, 121);
            this.textHospitalsRoomAddNumber.Name = "textHospitalsRoomAddNumber";
            this.textHospitalsRoomAddNumber.Size = new System.Drawing.Size(118, 22);
            this.textHospitalsRoomAddNumber.TabIndex = 6;
            // 
            // labelHospitalsRoomAddFloor
            // 
            this.labelHospitalsRoomAddFloor.AutoSize = true;
            this.labelHospitalsRoomAddFloor.Location = new System.Drawing.Point(255, 160);
            this.labelHospitalsRoomAddFloor.Name = "labelHospitalsRoomAddFloor";
            this.labelHospitalsRoomAddFloor.Size = new System.Drawing.Size(56, 17);
            this.labelHospitalsRoomAddFloor.TabIndex = 7;
            this.labelHospitalsRoomAddFloor.Text = "Piętro";
            // 
            // textHospitalsRoomAddFloor
            // 
            this.textHospitalsRoomAddFloor.Location = new System.Drawing.Point(364, 157);
            this.textHospitalsRoomAddFloor.Name = "textHospitalsRoomAddFloor";
            this.textHospitalsRoomAddFloor.Size = new System.Drawing.Size(118, 22);
            this.textHospitalsRoomAddFloor.TabIndex = 8;
            // 
            // labelHospitalsRoomAddHospitalId
            // 
            this.labelHospitalsRoomAddHospitalId.AutoSize = true;
            this.labelHospitalsRoomAddHospitalId.Location = new System.Drawing.Point(255, 198);
            this.labelHospitalsRoomAddHospitalId.Name = "labelHospitalsRoomAddHospitalId";
            this.labelHospitalsRoomAddHospitalId.Size = new System.Drawing.Size(52, 17);
            this.labelHospitalsRoomAddHospitalId.TabIndex = 9;
            this.labelHospitalsRoomAddHospitalId.Text = "Szpital";
            // 
            // comboHospitalsRoomAddHospitalId
            // 
            this.comboHospitalsRoomAddHospitalId.Location = new System.Drawing.Point(364, 195);
            this.comboHospitalsRoomAddHospitalId.Name = "textHospitalAddAddress";
            this.comboHospitalsRoomAddHospitalId.Size = new System.Drawing.Size(118, 22);
            this.comboHospitalsRoomAddHospitalId.TabIndex = 10;
            // 
            // tabControlHospitalsVisit
            // 
            this.tabControlHospitalsVisit.Controls.Add(this.tabPageHospitalsVisit);
            this.tabControlHospitalsVisit.Location = new System.Drawing.Point(140, 0);
            this.tabControlHospitalsVisit.Name = "tabControlHospitalsVisit";
            this.tabControlHospitalsVisit.SelectedIndex = 0;
            this.tabControlHospitalsVisit.Size = new System.Drawing.Size(729, 511);
            this.tabControlHospitalsVisit.TabIndex = 1;
            this.tabControlHospitalsVisit.Visible = false;
            // 
            // tabPageHospitalsVisit
            // 
            this.tabPageHospitalsVisit.Controls.Add(this.labelHospitalVisitTitle);
            this.tabPageHospitalsVisit.Controls.Add(this.dataGridHospitalVisit);
            this.tabPageHospitalsVisit.Location = new System.Drawing.Point(4, 25);
            this.tabPageHospitalsVisit.Name = "tabPageHospitalsVisit";
            this.tabPageHospitalsVisit.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHospitalsVisit.TabIndex = 0;
            this.tabPageHospitalsVisit.Text = "Szpital";
            this.tabPageHospitalsVisit.UseVisualStyleBackColor = true;
            // 
            // labelHospitalVisitTitle
            // 
            this.labelHospitalVisitTitle.AutoSize = true;
            this.labelHospitalVisitTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHospitalVisitTitle.Location = new System.Drawing.Point(203, 45);
            this.labelHospitalVisitTitle.Name = "labelHospitalVisitTitle";
            this.labelHospitalVisitTitle.Size = new System.Drawing.Size(179, 29);
            this.labelHospitalVisitTitle.TabIndex = 0;
            this.labelHospitalVisitTitle.Text = "";
            // 
            // dataGridHospitalVisit
            // 
            this.dataGridHospitalVisit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridHospitalVisit.Location = new System.Drawing.Point(37, 101);
            this.dataGridHospitalVisit.Name = "dataGridHospitalVisit";
            this.dataGridHospitalVisit.AllowUserToAddRows = false;
            this.dataGridHospitalVisit.RowTemplate.Height = 24;
            this.dataGridHospitalVisit.Size = new System.Drawing.Size(645, 292);
            this.dataGridHospitalVisit.TabIndex = 1;
            this.dataGridHospitalVisit.ColumnCount = 5;
            this.dataGridHospitalVisit.Columns[0].Name = "Termin";
            this.dataGridHospitalVisit.Columns[1].Name = "Czas trwania";
            this.dataGridHospitalVisit.Columns[2].Name = "Lekarz";
            this.dataGridHospitalVisit.Columns[3].Name = "Pacjent";
            this.dataGridHospitalVisit.Columns[4].Name = "Typ wizyty";
            // 
            // tabControlVisits
            // 
            this.tabControlVisits.Controls.Add(this.tabPageVisitsList);
            this.tabControlVisits.Location = new System.Drawing.Point(140, 0);
            this.tabControlVisits.Name = "tabControlVisits";
            this.tabControlVisits.SelectedIndex = 0;
            this.tabControlVisits.Size = new System.Drawing.Size(729, 511);
            this.tabControlVisits.TabIndex = 1;
            this.tabControlVisits.Visible = false;
            // 
            // tabPageVisitsList
            // 
            this.tabPageVisitsList.Controls.Add(this.dataGridVisit);
            this.tabPageVisitsList.Controls.Add(this.labelVisitListTitle);
            this.tabPageVisitsList.Controls.Add(this.buttonVisitAssign);
            this.tabPageVisitsList.Controls.Add(this.buttonVisitDelete);
            this.tabPageVisitsList.Location = new System.Drawing.Point(4, 25);
            this.tabPageVisitsList.Name = "tabPageVisitsList";
            this.tabPageVisitsList.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageVisitsList.TabIndex = 0;
            this.tabPageVisitsList.Text = "Lista wizyt";
            this.tabPageVisitsList.UseVisualStyleBackColor = true;
            // 
            // dataGridVisit
            // 
            this.dataGridVisit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridVisit.Location = new System.Drawing.Point(37, 101);
            this.dataGridVisit.Name = "dataGridVisit";
            this.dataGridVisit.AllowUserToAddRows = false;
            this.dataGridVisit.RowTemplate.Height = 24;
            this.dataGridVisit.Size = new System.Drawing.Size(645, 292);
            this.dataGridVisit.TabIndex = 1;
            this.dataGridVisit.ColumnCount = 6;
            this.dataGridVisit.Columns[0].Name = "Termin";
            this.dataGridVisit.Columns[1].Name = "Czas trwania";
            this.dataGridVisit.Columns[2].Name = "Lekarz";
            this.dataGridVisit.Columns[3].Name = "Sala";
            this.dataGridVisit.Columns[4].Name = "Pacjent";
            this.dataGridVisit.Columns[5].Name = "Typ wizyty";
            // 
            // labelVisitListTitle
            // 
            this.labelVisitListTitle.AutoSize = true;
            this.labelVisitListTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelVisitListTitle.Location = new System.Drawing.Point(276, 45);
            this.labelVisitListTitle.Name = "labelVisitListTitle";
            this.labelVisitListTitle.Size = new System.Drawing.Size(185, 29);
            this.labelVisitListTitle.TabIndex = 0;
            this.labelVisitListTitle.Text = "Lista wizyt";
            // 
            // buttonVisitAssign
            // 
            this.buttonVisitAssign.Location = new System.Drawing.Point(224, 399);
            this.buttonVisitAssign.Name = "buttonVisitAssign";
            this.buttonVisitAssign.Size = new System.Drawing.Size(128, 26);
            this.buttonVisitAssign.TabIndex = 11;
            this.buttonVisitAssign.Text = "Przydziel wizyty";
            this.buttonVisitAssign.UseVisualStyleBackColor = true;
            this.buttonVisitAssign.Click += new System.EventHandler(this.ButtonVisitAssign_Click);
            // 
            // buttonVisitDelete
            // 
            this.buttonVisitDelete.Location = new System.Drawing.Point(372, 399);
            this.buttonVisitDelete.Name = "buttonVisitDelete";
            this.buttonVisitDelete.Size = new System.Drawing.Size(88, 26);
            this.buttonVisitDelete.TabIndex = 11;
            this.buttonVisitDelete.Text = "Usuń";
            this.buttonVisitDelete.UseVisualStyleBackColor = true;
            this.buttonVisitDelete.Click += new System.EventHandler(this.ButtonVisitDelete_Click);
            //
            // panelScheduleAdd
            // 
            this.panelScheduleAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelScheduleAdd.Controls.Add(this.buttonScheduleSave);
            this.panelScheduleAdd.Controls.Add(this.labelScheduleStaffName);
            this.panelScheduleAdd.Controls.Add(this.checkMonday);
            this.panelScheduleAdd.Controls.Add(this.checkTuesday);
            this.panelScheduleAdd.Controls.Add(this.checkWednesday);
            this.panelScheduleAdd.Controls.Add(this.checkThursday);
            this.panelScheduleAdd.Controls.Add(this.checkFriday);
            this.panelScheduleAdd.Controls.Add(this.comboMondayFrom);
            this.panelScheduleAdd.Controls.Add(this.comboMondayTo);
            this.panelScheduleAdd.Controls.Add(this.comboTuesdayFrom);
            this.panelScheduleAdd.Controls.Add(this.comboTuesdayTo);
            this.panelScheduleAdd.Controls.Add(this.comboWednesdayFrom);
            this.panelScheduleAdd.Controls.Add(this.comboWednesdayTo);
            this.panelScheduleAdd.Controls.Add(this.comboThursdayFrom);
            this.panelScheduleAdd.Controls.Add(this.comboThursdayTo);
            this.panelScheduleAdd.Controls.Add(this.comboFridayFrom);
            this.panelScheduleAdd.Controls.Add(this.comboFridayTo);
            this.panelScheduleAdd.Location = new System.Drawing.Point(285, 88);
            this.panelScheduleAdd.Name = "panelScheduleAdd";
            this.panelScheduleAdd.Size = new System.Drawing.Size(467, 347);
            this.panelScheduleAdd.TabIndex = 12;
            this.panelScheduleAdd.Visible = false;
            // 
            // buttonScheduleSave
            // 
            this.buttonScheduleSave.Location = new System.Drawing.Point(177, 263);
            this.buttonScheduleSave.Name = "buttonScheduleSave";
            this.buttonScheduleSave.Size = new System.Drawing.Size(75, 26);
            this.buttonScheduleSave.TabIndex = 21;
            this.buttonScheduleSave.Text = "Zapisz";
            this.buttonScheduleSave.UseVisualStyleBackColor = true;
            this.buttonScheduleSave.Click += new System.EventHandler(this.ButtonScheduleSave_Click);
            // 
            // labelScheduleStaffName
            // 
            this.labelScheduleStaffName.AutoSize = true;
            this.labelScheduleStaffName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelScheduleStaffName.Location = new System.Drawing.Point(58, 29);
            this.labelScheduleStaffName.Name = "labelScheduleStaffName";
            this.labelScheduleStaffName.Size = new System.Drawing.Size(46, 17);
            this.labelScheduleStaffName.TabIndex = 0;
            this.labelScheduleStaffName.Text = "";
            // 
            // checkMonday
            // 
            this.checkMonday.AutoSize = true;
            this.checkMonday.Location = new System.Drawing.Point(54, 75);
            this.checkMonday.Name = "checkMonday";
            this.checkMonday.Size = new System.Drawing.Size(110, 21);
            this.checkMonday.TabIndex = 6;
            this.checkMonday.Text = "Poniedziałek";
            this.checkMonday.UseVisualStyleBackColor = true;
            this.checkMonday.Click += new System.EventHandler(this.CheckMonday_Click);
            // 
            // checkTuesday
            // 
            this.checkTuesday.AutoSize = true;
            this.checkTuesday.Location = new System.Drawing.Point(54, 112);
            this.checkTuesday.Name = "checkTuesday";
            this.checkTuesday.Size = new System.Drawing.Size(75, 21);
            this.checkTuesday.TabIndex = 7;
            this.checkTuesday.Text = "Wtorek";
            this.checkTuesday.UseVisualStyleBackColor = true;
            this.checkTuesday.Click += new System.EventHandler(this.CheckTuesday_Click);
            // 
            // checkWednesday
            // 
            this.checkWednesday.AutoSize = true;
            this.checkWednesday.Location = new System.Drawing.Point(54, 150);
            this.checkWednesday.Name = "checkWednesday";
            this.checkWednesday.Size = new System.Drawing.Size(68, 21);
            this.checkWednesday.TabIndex = 8;
            this.checkWednesday.Text = "Środa";
            this.checkWednesday.UseVisualStyleBackColor = true;
            this.checkWednesday.Click += new System.EventHandler(this.CheckWednesday_Click);
            // 
            // checkThursday
            // 
            this.checkThursday.AutoSize = true;
            this.checkThursday.Location = new System.Drawing.Point(54, 187);
            this.checkThursday.Name = "checkThursday";
            this.checkThursday.Size = new System.Drawing.Size(87, 21);
            this.checkThursday.TabIndex = 9;
            this.checkThursday.Text = "Czwartek";
            this.checkThursday.UseVisualStyleBackColor = true;
            this.checkThursday.Click += new System.EventHandler(this.CheckThursday_Click);
            // 
            // checkFriday
            // 
            this.checkFriday.AutoSize = true;
            this.checkFriday.Location = new System.Drawing.Point(54, 222);
            this.checkFriday.Name = "checkFriday";
            this.checkFriday.Size = new System.Drawing.Size(69, 21);
            this.checkFriday.TabIndex = 10;
            this.checkFriday.Text = "Piątek";
            this.checkFriday.UseVisualStyleBackColor = true;
            this.checkFriday.Click += new System.EventHandler(this.CheckFriday_Click);
            // 
            // comboMondayFrom
            // 
            this.comboMondayFrom.FormattingEnabled = true;
            this.comboMondayFrom.Location = new System.Drawing.Point(183, 72);
            this.comboMondayFrom.Name = "comboMondayFrom";
            this.comboMondayFrom.Size = new System.Drawing.Size(83, 24);
            this.comboMondayFrom.TabIndex = 11;
            this.comboMondayFrom.Enabled = false;
            this.comboMondayFrom.Items.AddRange(new string[12] { "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00" });
            // 
            // comboMondayTo
            // 
            this.comboMondayTo.FormattingEnabled = true;
            this.comboMondayTo.Location = new System.Drawing.Point(293, 71);
            this.comboMondayTo.Name = "comboMondayTo";
            this.comboMondayTo.Size = new System.Drawing.Size(83, 24);
            this.comboMondayTo.TabIndex = 16;
            this.comboMondayTo.Enabled = false;
            this.comboMondayTo.Items.AddRange(new string[12] { "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00" });
            // 
            // comboTuesdayFrom
            // 
            this.comboTuesdayFrom.FormattingEnabled = true;
            this.comboTuesdayFrom.Location = new System.Drawing.Point(182, 109);
            this.comboTuesdayFrom.Name = "comboTuesdayFrom";
            this.comboTuesdayFrom.Size = new System.Drawing.Size(84, 24);
            this.comboTuesdayFrom.TabIndex = 12;
            this.comboTuesdayFrom.Enabled = false;
            this.comboTuesdayFrom.Items.AddRange(new string[12] { "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00" });
            // 
            // comboTuesdayTo
            // 
            this.comboTuesdayTo.FormattingEnabled = true;
            this.comboTuesdayTo.Location = new System.Drawing.Point(293, 108);
            this.comboTuesdayTo.Name = "comboTuesdayTo";
            this.comboTuesdayTo.Size = new System.Drawing.Size(83, 24);
            this.comboTuesdayTo.TabIndex = 17;
            this.comboTuesdayTo.Enabled = false;
            this.comboTuesdayTo.Items.AddRange(new string[12] { "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00" });
            // 
            // comboWednesdayFrom
            // 
            this.comboWednesdayFrom.FormattingEnabled = true;
            this.comboWednesdayFrom.Location = new System.Drawing.Point(182, 147);
            this.comboWednesdayFrom.Name = "comboWednesdayFrom";
            this.comboWednesdayFrom.Size = new System.Drawing.Size(84, 24);
            this.comboWednesdayFrom.TabIndex = 13;
            this.comboWednesdayFrom.Enabled = false;
            this.comboWednesdayFrom.Items.AddRange(new string[12] { "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00" });
            // 
            // comboWednesdayTo
            // 
            this.comboWednesdayTo.FormattingEnabled = true;
            this.comboWednesdayTo.Location = new System.Drawing.Point(293, 147);
            this.comboWednesdayTo.Name = "comboWednesdayTo";
            this.comboWednesdayTo.Size = new System.Drawing.Size(83, 24);
            this.comboWednesdayTo.TabIndex = 18;
            this.comboWednesdayTo.Enabled = false;
            this.comboWednesdayTo.Items.AddRange(new string[12] { "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00" });
            // 
            // comboThursdayFrom
            // 
            this.comboThursdayFrom.FormattingEnabled = true;
            this.comboThursdayFrom.Location = new System.Drawing.Point(182, 184);
            this.comboThursdayFrom.Name = "comboThursdayFrom";
            this.comboThursdayFrom.Size = new System.Drawing.Size(84, 24);
            this.comboThursdayFrom.TabIndex = 14;
            this.comboThursdayFrom.Enabled = false;
            this.comboThursdayFrom.Items.AddRange(new string[12] { "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00" });
            // 
            // comboThursdayTo
            // 
            this.comboThursdayTo.FormattingEnabled = true;
            this.comboThursdayTo.Location = new System.Drawing.Point(293, 184);
            this.comboThursdayTo.Name = "comboThursdayTo";
            this.comboThursdayTo.Size = new System.Drawing.Size(83, 24);
            this.comboThursdayTo.TabIndex = 19;
            this.comboThursdayTo.Enabled = false;
            this.comboThursdayTo.Items.AddRange(new string[12] { "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00" });
            // 
            // comboFridayFrom
            // 
            this.comboFridayFrom.FormattingEnabled = true;
            this.comboFridayFrom.Location = new System.Drawing.Point(182, 219);
            this.comboFridayFrom.Name = "comboFridayFrom";
            this.comboFridayFrom.Size = new System.Drawing.Size(84, 24);
            this.comboFridayFrom.TabIndex = 15;
            this.comboFridayFrom.Enabled = false;
            this.comboFridayFrom.Items.AddRange(new string[12] { "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00" });
            // 
            // comboFridayTo
            // 
            this.comboFridayTo.FormattingEnabled = true;
            this.comboFridayTo.Location = new System.Drawing.Point(293, 219);
            this.comboFridayTo.Name = "comboFridayTo";
            this.comboFridayTo.Size = new System.Drawing.Size(83, 24);
            this.comboFridayTo.TabIndex = 20;
            this.comboFridayTo.Enabled = false;
            this.comboFridayTo.Items.AddRange(new string[12] { "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00" });
            //
            // panelVisitAdd
            // 
            this.panelVisitAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVisitAdd.Controls.Add(this.buttonVisitSave);
            this.panelVisitAdd.Controls.Add(this.labelVisitAddTitle);
            this.panelVisitAdd.Controls.Add(this.labelVisitAddDate);
            this.panelVisitAdd.Controls.Add(this.pickerVisitAddDate);
            this.panelVisitAdd.Controls.Add(this.labelVisitAddHours);
            this.panelVisitAdd.Controls.Add(this.comboVisitAddHours);
            this.panelVisitAdd.Controls.Add(this.labelVisitAddHospital);
            this.panelVisitAdd.Controls.Add(this.comboVisitAddHospital);
            this.panelVisitAdd.Controls.Add(this.labelVisitAddType);
            this.panelVisitAdd.Controls.Add(this.comboVisitAddType);
            this.panelVisitAdd.Location = new System.Drawing.Point(285, 88);
            this.panelVisitAdd.Name = "panelVisitAdd";
            this.panelVisitAdd.Size = new System.Drawing.Size(467, 327);
            this.panelVisitAdd.TabIndex = 12;
            this.panelVisitAdd.Visible = false;
            // 
            // buttonVisitSave
            // 
            this.buttonVisitSave.Location = new System.Drawing.Point(177, 253);
            this.buttonVisitSave.Name = "buttonVisitSave";
            this.buttonVisitSave.Size = new System.Drawing.Size(95, 26);
            this.buttonVisitSave.TabIndex = 21;
            this.buttonVisitSave.Text = "Rezerwuj";
            this.buttonVisitSave.UseVisualStyleBackColor = true;
            this.buttonVisitSave.Click += new System.EventHandler(this.ButtonVisitSave_Click);
            // 
            // labelVisitAddTitle
            // 
            this.labelVisitAddTitle.AutoSize = true;
            this.labelVisitAddTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelVisitAddTitle.Location = new System.Drawing.Point(123, 29);
            this.labelVisitAddTitle.Name = "labelVisitAddTitle";
            this.labelVisitAddTitle.Size = new System.Drawing.Size(46, 17);
            this.labelVisitAddTitle.TabIndex = 0;
            this.labelVisitAddTitle.Text = "";
            // 
            // labelVisitAddDate
            // 
            this.labelVisitAddDate.AutoSize = true;
            this.labelVisitAddDate.Location = new System.Drawing.Point(104, 85);
            this.labelVisitAddDate.Name = "labelVisitAddDate";
            this.labelVisitAddDate.Size = new System.Drawing.Size(56, 17);
            this.labelVisitAddDate.TabIndex = 1;
            this.labelVisitAddDate.Text = "Data wizyty";
            // 
            // pickerVisitAddDate
            // 
            this.pickerVisitAddDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.pickerVisitAddDate.Location = new System.Drawing.Point(242, 82);
            this.pickerVisitAddDate.MinDate = System.DateTime.Today.AddDays(1);
            this.pickerVisitAddDate.MaxDate = System.DateTime.Today.AddMonths(2);
            this.pickerVisitAddDate.Name = "pickerVisitAddDate";
            this.pickerVisitAddDate.Size = new System.Drawing.Size(124, 22);
            this.pickerVisitAddDate.TabIndex = 2;
            // 
            // labelVisitAddHours
            // 
            this.labelVisitAddHours.AutoSize = true;
            this.labelVisitAddHours.Location = new System.Drawing.Point(104, 123);
            this.labelVisitAddHours.Name = "labelVisitAddHours";
            this.labelVisitAddHours.Size = new System.Drawing.Size(56, 17);
            this.labelVisitAddHours.TabIndex = 3;
            this.labelVisitAddHours.Text = "Godzina wizyty";
            // 
            // comboVisitAddHours
            // 
            this.comboVisitAddHours.Location = new System.Drawing.Point(242, 120);
            this.comboVisitAddHours.Name = "comboVisitAddHours";
            this.comboVisitAddHours.Size = new System.Drawing.Size(124, 24);
            this.comboVisitAddHours.TabIndex = 4;
            this.comboVisitAddHours.Items.AddRange(new string[3] { "08:00 - 12:00", "12:00 - 16:00", "16:00 - 20:00" });
            // 
            // labelVisitAddHospital
            // 
            this.labelVisitAddHospital.AutoSize = true;
            this.labelVisitAddHospital.Location = new System.Drawing.Point(104, 161);
            this.labelVisitAddHospital.Name = "labelVisitAddHospital";
            this.labelVisitAddHospital.Size = new System.Drawing.Size(56, 17);
            this.labelVisitAddHospital.TabIndex = 5;
            this.labelVisitAddHospital.Text = "Szpital";
            // 
            // comboVisitAddHospital
            // 
            this.comboVisitAddHospital.FormattingEnabled = true;
            this.comboVisitAddHospital.Location = new System.Drawing.Point(242, 158);
            this.comboVisitAddHospital.Name = "comboVisitAddHospital";
            this.comboVisitAddHospital.Size = new System.Drawing.Size(124, 24);
            this.comboVisitAddHospital.TabIndex = 6;
            // 
            // labelVisitAddType
            // 
            this.labelVisitAddType.AutoSize = true;
            this.labelVisitAddType.Location = new System.Drawing.Point(104, 199);
            this.labelVisitAddType.Name = "labelVisitAddType";
            this.labelVisitAddType.Size = new System.Drawing.Size(56, 17);
            this.labelVisitAddType.TabIndex = 7;
            this.labelVisitAddType.Text = "Rodzaj wizyty";
            // 
            // comboVisitAddType
            // 
            this.comboVisitAddType.FormattingEnabled = true;
            this.comboVisitAddType.Location = new System.Drawing.Point(242, 196);
            this.comboVisitAddType.Name = "comboVisitAddType";
            this.comboVisitAddType.Size = new System.Drawing.Size(124, 24);
            this.comboVisitAddType.TabIndex = 8;
            this.comboVisitAddType.Items.AddRange(new string[4] { "Konsultacja", "Zabieg", "Operacja", "Badanie" });
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 511);
            this.Controls.Add(this.panelSideNav);
            this.Controls.Add(this.tabControlPatients);
            this.Controls.Add(this.tabControlPatientsVisit);
            this.Controls.Add(this.tabControlStaff);
            this.Controls.Add(this.tabControlStaffVisit);
            this.Controls.Add(this.tabControlHospitals);
            this.Controls.Add(this.tabControlHospitalsVisit);
            this.Controls.Add(this.tabControlVisits);
            this.Controls.Add(this.panelScheduleAdd);
            this.Controls.Add(this.panelVisitAdd);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zarządzanie szpitalem";
            this.panelSideNav.ResumeLayout(false);
            this.tabControlPatients.ResumeLayout(false);
            this.tabControlPatientsVisit.ResumeLayout(false);
            this.tabControlStaff.ResumeLayout(false);
            this.tabControlStaffVisit.ResumeLayout(false);
            this.tabControlHospitals.ResumeLayout(false);
            this.tabControlHospitalsVisit.ResumeLayout(false);
            this.tabControlVisits.ResumeLayout(false);
            this.panelScheduleAdd.ResumeLayout(false);
            this.panelVisitAdd.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private System.Windows.Forms.Panel panelSideNav;
        private System.Windows.Forms.Button buttonVisits;
        private System.Windows.Forms.Button buttonHospital;
        private System.Windows.Forms.Button buttonStaff;
        private System.Windows.Forms.Button buttonPatients;

        private System.Windows.Forms.TabControl tabControlPatients;
        private System.Windows.Forms.TabPage tabPagePatientsList;
        private System.Windows.Forms.Label labelPatientListTitle;
        private System.Windows.Forms.DataGridView dataGridPatient;
        private System.Windows.Forms.Button buttonPatientChoose;
        private System.Windows.Forms.Button buttonPatientDelete;
        private System.Windows.Forms.TabPage tabPagePatientsAdd;
        private System.Windows.Forms.Button buttonPatientAdd;
        private System.Windows.Forms.Label labelPatientAddTitle;
        private System.Windows.Forms.Label labelPatientAddName;
        private System.Windows.Forms.TextBox textPatientAddName;
        private System.Windows.Forms.Label labelPatientAddSurname;
        private System.Windows.Forms.TextBox textPatientAddSurname;
        private System.Windows.Forms.Label labelPatientAddPesel;
        private System.Windows.Forms.TextBox textPatientAddPesel;
        private System.Windows.Forms.Label labelPatientAddPhone;
        private System.Windows.Forms.TextBox textPatientAddPhone;
        private System.Windows.Forms.Label labelPatientAddCity;
        private System.Windows.Forms.TextBox textPatientAddCity;

        private System.Windows.Forms.TabControl tabControlPatientsVisit;
        private System.Windows.Forms.TabPage tabPagePatientsVisit;
        private System.Windows.Forms.Label labelPatientVisitTitle;
        private System.Windows.Forms.DataGridView dataGridPatientVisit;
        private System.Windows.Forms.Button buttonPatientVisitBook;

        private System.Windows.Forms.TabControl tabControlStaff;
        private System.Windows.Forms.TabPage tabPageStaffList;
        private System.Windows.Forms.Label labelStaffListTitle;
        private System.Windows.Forms.DataGridView dataGridStaff;
        private System.Windows.Forms.Button buttonStaffChoose;
        private System.Windows.Forms.Button buttonStaffDelete;
        private System.Windows.Forms.TabPage tabPageStaffAdd;
        private System.Windows.Forms.Button buttonStaffAdd;
        private System.Windows.Forms.Label labelStaffAddTitle;
        private System.Windows.Forms.Label labelStaffAddName;
        private System.Windows.Forms.TextBox textStaffAddName;
        private System.Windows.Forms.Label labelStaffAddSurname;
        private System.Windows.Forms.TextBox textStaffAddSurname;
        private System.Windows.Forms.Label labelStaffAddPesel;
        private System.Windows.Forms.TextBox textStaffAddPesel;
        private System.Windows.Forms.Label labelStaffAddPhone;
        private System.Windows.Forms.TextBox textStaffAddPhone;
        private System.Windows.Forms.Label labelStaffAddCity;
        private System.Windows.Forms.TextBox textStaffAddCity;
        private System.Windows.Forms.Label labelStaffAddPosition;
        private System.Windows.Forms.ComboBox comboStaffAddPosition;
        private System.Windows.Forms.TabPage tabPageStaffPositionAdd;
        private System.Windows.Forms.Button buttonStaffPositionAdd;
        private System.Windows.Forms.Label labelStaffPositionAddTitle;
        private System.Windows.Forms.Label labelStaffPositionAddJob;
        private System.Windows.Forms.ComboBox comboStaffPositionAddJob;
        private System.Windows.Forms.Label labelStaffPositionAddSpecialization;
        private System.Windows.Forms.TextBox textStaffPositionAddSpecialization;

        private System.Windows.Forms.TabControl tabControlStaffVisit;
        private System.Windows.Forms.TabPage tabPageStaffVisit;
        private System.Windows.Forms.Label labelStaffVisitTitle;
        private System.Windows.Forms.DataGridView dataGridStaffVisit;

        private System.Windows.Forms.TabControl tabControlHospitals;
        private System.Windows.Forms.TabPage tabPageHospitalsList;
        private System.Windows.Forms.Label labelHospitalListTitle;
        private System.Windows.Forms.DataGridView dataGridHospital;
        private System.Windows.Forms.Button buttonHospitalChoose;
        private System.Windows.Forms.Button buttonHospitalDelete;
        private System.Windows.Forms.TabPage tabPageHospitalsAdd;
        private System.Windows.Forms.Button buttonHospitalAdd;
        private System.Windows.Forms.Label labelHospitalAddTitle;
        private System.Windows.Forms.Label labelHospitalAddName;
        private System.Windows.Forms.TextBox textHospitalAddName;
        private System.Windows.Forms.Label labelHospitalAddCity;
        private System.Windows.Forms.TextBox textHospitalAddCity;
        private System.Windows.Forms.Label labelHospitalAddAddress;
        private System.Windows.Forms.TextBox textHospitalAddAddress;
        private System.Windows.Forms.TabPage tabPageHospitalsRoomAdd;
        private System.Windows.Forms.Button buttonHospitalsRoomAdd;
        private System.Windows.Forms.Label labelHospitalsRoomAddTitle;
        private System.Windows.Forms.Label labelHospitalsRoomAddNumber;
        private System.Windows.Forms.TextBox textHospitalsRoomAddNumber;
        private System.Windows.Forms.Label labelHospitalsRoomAddFloor;
        private System.Windows.Forms.TextBox textHospitalsRoomAddFloor;
        private System.Windows.Forms.Label labelHospitalsRoomAddHospitalId;
        private System.Windows.Forms.ComboBox comboHospitalsRoomAddHospitalId;

        private System.Windows.Forms.TabControl tabControlHospitalsVisit;
        private System.Windows.Forms.TabPage tabPageHospitalsVisit;
        private System.Windows.Forms.Label labelHospitalVisitTitle;
        private System.Windows.Forms.DataGridView dataGridHospitalVisit;

        private System.Windows.Forms.TabControl tabControlVisits;
        private System.Windows.Forms.TabPage tabPageVisitsList;
        private System.Windows.Forms.Label labelVisitListTitle;
        private System.Windows.Forms.DataGridView dataGridVisit;
        private System.Windows.Forms.Button buttonVisitAssign;
        private System.Windows.Forms.Button buttonVisitDelete;

        private System.Windows.Forms.Panel panelScheduleAdd;
        private System.Windows.Forms.Label labelScheduleStaffName;
        private System.Windows.Forms.ComboBox comboFridayTo;
        private System.Windows.Forms.ComboBox comboThursdayTo;
        private System.Windows.Forms.ComboBox comboWednesdayTo;
        private System.Windows.Forms.ComboBox comboTuesdayTo;
        private System.Windows.Forms.ComboBox comboMondayTo;
        private System.Windows.Forms.ComboBox comboFridayFrom;
        private System.Windows.Forms.ComboBox comboThursdayFrom;
        private System.Windows.Forms.ComboBox comboWednesdayFrom;
        private System.Windows.Forms.ComboBox comboTuesdayFrom;
        private System.Windows.Forms.ComboBox comboMondayFrom;
        private System.Windows.Forms.CheckBox checkFriday;
        private System.Windows.Forms.CheckBox checkThursday;
        private System.Windows.Forms.CheckBox checkWednesday;
        private System.Windows.Forms.CheckBox checkTuesday;
        private System.Windows.Forms.CheckBox checkMonday;
        private System.Windows.Forms.Button buttonScheduleSave;

        private System.Windows.Forms.Panel panelVisitAdd;
        private System.Windows.Forms.Label labelVisitAddTitle;
        private System.Windows.Forms.Label labelVisitAddDate;
        private System.Windows.Forms.DateTimePicker pickerVisitAddDate;
        private System.Windows.Forms.Label labelVisitAddHours;
        private System.Windows.Forms.ComboBox comboVisitAddHours;
        private System.Windows.Forms.Label labelVisitAddHospital;
        private System.Windows.Forms.ComboBox comboVisitAddHospital;
        private System.Windows.Forms.Label labelVisitAddType;
        private System.Windows.Forms.ComboBox comboVisitAddType;
        private System.Windows.Forms.Button buttonVisitSave;
    }
}

