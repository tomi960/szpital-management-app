﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace szpital_management
{
    class SA
    {
        public static void SimulatedAnnealing()
        {
            var visitList = Wizyta.GetWizytasList();
            Random r = new Random();
            double startTemperature = 100;
            double endTemperature = 0.005;
            double temperature = startTemperature;
            double lambda = 0.999;
            double initialCost = CalculateCostFunction(visitList);
            Tuple<List<Wizyta>, double> bestSolution = new Tuple<List<Wizyta>, double>(visitList.ConvertAll(v => new Wizyta(v)), initialCost);
            Tuple<List<Wizyta>, double> currentSolution = new Tuple<List<Wizyta>, double>(visitList.ConvertAll(v => new Wizyta(v)), initialCost);
            int iterationsCount = 0;
            while (temperature > endTemperature || bestSolution.Item2 == 0)
            {
                List<Wizyta> newVisitList = GenerateNewPermutationWithPacjentsCheck(currentSolution.Item1).ToList().ConvertAll(v => new Wizyta(v));
                Tuple<List<Wizyta>, double> newSolution = new Tuple<List<Wizyta>, double>(newVisitList, CalculateCostFunction(newVisitList)); // liczenie funkcji celu trwa długo
                if (newSolution.Item2 < bestSolution.Item2)
                    bestSolution = new Tuple<List<Wizyta>, double>(newSolution.Item1.ConvertAll(v => new Wizyta(v)), newSolution.Item2);
                if (newSolution.Item2 <= currentSolution.Item2)
                    currentSolution = new Tuple<List<Wizyta>, double>(newSolution.Item1.ConvertAll(v => new Wizyta(v)), newSolution.Item2);
                else
                {
                    double delta = newSolution.Item2 - currentSolution.Item2;
                    double prob = Math.Exp(-delta / temperature);
                    double z = r.NextDouble();
                    if (z <= prob)
                    {
                        currentSolution = new Tuple<List<Wizyta>, double>(newSolution.Item1.ConvertAll(v => new Wizyta(v)), newSolution.Item2);
                    }
                }
                iterationsCount++;
                Console.WriteLine(iterationsCount + " " + currentSolution.Item2 + " " + bestSolution.Item2);
                temperature *= lambda;
            }
            foreach (Wizyta v in bestSolution.Item1)
            {
                Wizyta.UpdateWizyta(v);
            }
            MessageBox.Show("Liczba iteracji: " + iterationsCount.ToString() + "\n" +
            "Poczatkowa wartosc funkcji celu: " + initialCost.ToString() + "\n" +
            "Koncowa wartosc funkcji celu: " + bestSolution.Item2.ToString());
        }

        public static void InitialSetup()
        {
            var visitList = Wizyta.GetUnassignedWizytasList();
            var staffList = Kadra.GetKadrasList();
            var staffListCount = staffList.Count;
            Random r = new Random();
            foreach (Wizyta w in visitList)
            {
                bool visitSet = false;
                var randomId = r.Next(0, staffListCount);
                var firstRandomId = randomId;
                while (!visitSet) // przerywane, gdy wizyta zostanie ustawiona
                {
                    var staff = staffList[randomId % staffListCount];
                    var staffVisits = Wizyta.GetWizytasByDoctorList(staff.Id).Where(v => v.Zarejestrowana == true && v.Termin.Date == w.Termin.Date).ToList(); // tylko wizyty zarejestrowane i w tym samym dniu
                    staffVisits = staffVisits.Where(v => v.Termin >= w.Termin && v.Termin.AddHours(v.CzasTrwania.Hours).AddMinutes(v.CzasTrwania.Minutes) <= w.Termin.AddHours(4)).ToList(); // tylko wizyty z wybranego przedzialu
                    if (staffVisits.Count == 0)
                    {
                        w.Lekarz_Id = staff.Id;
                        w.Zarejestrowana = true;
                        Wizyta.UpdateWizyta(w);
                        visitSet = true;
                    }
                    else if (staffVisits.Count == 1)
                    {
                        DateTime nowaWizytaPoczatek = w.Termin;
                        DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(w.CzasTrwania.Hours).AddMinutes(w.CzasTrwania.Minutes);
                        DateTime wizytaPoczatek = staffVisits[0].Termin;
                        DateTime wizytaKoniec = wizytaPoczatek.AddHours(staffVisits[0].CzasTrwania.Hours).AddMinutes(staffVisits[0].CzasTrwania.Minutes);
                        while (visitSet == false && nowaWizytaKoniec <= w.Termin.AddHours(4)) // czy miesci sie w przedziale
                        {
                            if ((nowaWizytaPoczatek < wizytaPoczatek && nowaWizytaKoniec <= wizytaPoczatek) ||
                                (nowaWizytaPoczatek >= wizytaKoniec && nowaWizytaKoniec > wizytaKoniec))
                            {
                                w.Termin = nowaWizytaPoczatek;
                                w.Lekarz_Id = staff.Id;
                                w.Zarejestrowana = true;
                                Wizyta.UpdateWizyta(w);
                                visitSet = true;
                            }
                            nowaWizytaPoczatek = nowaWizytaKoniec;
                            nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(w.CzasTrwania.Hours).AddMinutes(w.CzasTrwania.Minutes);
                        }
                    }
                    else
                    {
                        DateTime nowaWizytaPoczatek = w.Termin;
                        DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(w.CzasTrwania.Hours).AddMinutes(w.CzasTrwania.Minutes);
                        for (int i = 0; (i < staffVisits.Count && visitSet == false); i++)
                        {
                            DateTime wizytaPoczatek = staffVisits[i].Termin;
                            DateTime wizytaKoniec = wizytaPoczatek.AddHours(staffVisits[i].CzasTrwania.Hours).AddMinutes(staffVisits[i].CzasTrwania.Minutes);
                            if (i == 0) // jesli pierwsza wizyta z listy
                            {
                                if (nowaWizytaPoczatek < wizytaPoczatek && nowaWizytaKoniec <= wizytaPoczatek)
                                {
                                    w.Lekarz_Id = staff.Id;
                                    w.Zarejestrowana = true;
                                    Wizyta.UpdateWizyta(w);
                                    visitSet = true;
                                }
                            }
                            else if (i == staffVisits.Count - 1) // jesli ostatnia wizyta z listy
                            {
                                if (wizytaKoniec.AddHours(w.CzasTrwania.Hours).AddMinutes(w.CzasTrwania.Minutes) <= w.Termin.AddHours(4))
                                {
                                    w.Termin = wizytaKoniec;
                                    w.Lekarz_Id = staff.Id;
                                    w.Zarejestrowana = true;
                                    Wizyta.UpdateWizyta(w);
                                    visitSet = true;
                                }
                            }
                            else
                            {
                                DateTime nastepnaWizytaPoczatek = staffVisits[i + 1].Termin;
                                DateTime nastepnaWizytaKoniec = nastepnaWizytaPoczatek.AddHours(staffVisits[i + 1].CzasTrwania.Hours).AddMinutes(staffVisits[i + 1].CzasTrwania.Minutes);
                                DateTime czasPrzerwy = nastepnaWizytaPoczatek.AddHours(-wizytaKoniec.Hour).AddMinutes(-wizytaKoniec.Minute);
                                if (czasPrzerwy.Hour > w.CzasTrwania.Hours || (czasPrzerwy.Hour == w.CzasTrwania.Hours && czasPrzerwy.Minute >= w.CzasTrwania.Minutes))
                                {
                                    w.Termin = wizytaKoniec;
                                    w.Lekarz_Id = staff.Id;
                                    w.Zarejestrowana = true;
                                    Wizyta.UpdateWizyta(w);
                                    visitSet = true;
                                }
                            }
                        }
                    }
                    randomId++;
                    if (randomId % staffListCount == firstRandomId % staffListCount)
                        break;
                }
            }
        }

        private static double CalculateCostFunction(List<Wizyta> visitList)
        {
            var staffList = Kadra.GetKadrasList();
            double staffCostSum = 0;
            foreach (Kadra k in staffList)
            {
                var staffVisitList = visitList.Where(w => w.Lekarz_Id == k.Id).OrderBy(w => w.Termin).ToList();
                for (int i = 1; i < staffVisitList.Count; i++)
                {
                    if (staffVisitList[i].Termin.Date == staffVisitList[i - 1].Termin.Date)
                    {
                        var earlierVisitEnd = staffVisitList[i - 1].Termin.AddHours(staffVisitList[i - 1].CzasTrwania.Hours).AddMinutes(staffVisitList[i - 1].CzasTrwania.Minutes);
                        var timeDifference = staffVisitList[i].Termin.AddHours(-earlierVisitEnd.Hour).AddMinutes(-earlierVisitEnd.Minute);
                        staffCostSum += timeDifference.Hour + timeDifference.Minute / 60.0;
                    }
                }
            }
            return staffCostSum;
        }

        private static List<Wizyta> GenerateNewPermutation(List<Wizyta> visitList)
        {
            var staffList = Kadra.GetKadrasList();
            var staffListCount = staffList.Count;
            Random r = new Random();
            var randomVisitId = r.Next(0, visitList.Count);
            var randomStaffId = r.Next(0, staffListCount);
            bool visitSet = false;
            var firstRandomStaffId = randomStaffId;
            while (!visitSet)
            {
                var randomVisit = visitList[randomVisitId];
                var randomStaff = staffList[randomStaffId % staffListCount];
                var slot = GetSlotBeginning(randomVisit.Termin);
                var staffVisits = visitList.Where(v => v.Lekarz_Id == randomStaff.Id && v.Termin.Date == randomVisit.Termin.Date).OrderBy(v => v.Termin).ToList(); // tylko wizyty w tym samym dniu
                staffVisits = staffVisits.Where(v => v.Termin >= slot && v.Termin.AddHours(v.CzasTrwania.Hours).AddMinutes(v.CzasTrwania.Minutes) <= slot.AddHours(4)).ToList(); // tylko wizyty z danego przedzialu
                List<DateTime> swapPossibilities = new List<DateTime>();
                if (staffVisits.Count == 0)
                {
                    DateTime nowaWizytaPoczatek = slot;
                    DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                    while (nowaWizytaKoniec <= slot.AddHours(4))
                    {
                        swapPossibilities.Add(nowaWizytaPoczatek);
                        nowaWizytaPoczatek = nowaWizytaKoniec;
                        nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                    }
                }
                else if (staffVisits.Count == 1)
                {
                    DateTime nowaWizytaPoczatek = slot;
                    DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                    DateTime wizytaPoczatek = staffVisits[0].Termin;
                    DateTime wizytaKoniec = wizytaPoczatek.AddHours(staffVisits[0].CzasTrwania.Hours).AddMinutes(staffVisits[0].CzasTrwania.Minutes);
                    while (nowaWizytaKoniec <= slot.AddHours(4))
                    {
                        if ((nowaWizytaPoczatek < wizytaPoczatek && nowaWizytaKoniec <= wizytaPoczatek) ||
                            (nowaWizytaPoczatek >= wizytaKoniec && nowaWizytaKoniec > wizytaKoniec))
                        {
                            swapPossibilities.Add(nowaWizytaPoczatek);
                        }
                        nowaWizytaPoczatek = nowaWizytaKoniec;
                        nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                    }
                }
                else
                {
                    for (int i = 0; i < staffVisits.Count; i++)
                    {
                        DateTime wizytaPoczatek = staffVisits[i].Termin;
                        DateTime wizytaKoniec = wizytaPoczatek.AddHours(staffVisits[i].CzasTrwania.Hours).AddMinutes(staffVisits[i].CzasTrwania.Minutes);
                        if (i == 0) // jesli pierwsza wizyta z listy
                        {
                            DateTime nowaWizytaPoczatek = slot;
                            DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                            while (nowaWizytaKoniec <= wizytaPoczatek)
                            {
                                swapPossibilities.Add(nowaWizytaPoczatek);
                                nowaWizytaPoczatek = nowaWizytaKoniec;
                                nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                            }
                        }
                        else if (i == staffVisits.Count - 1) // jesli ostatnia wizyta z listy
                        {
                            DateTime nowaWizytaPoczatek = wizytaKoniec;
                            DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                            while (nowaWizytaKoniec <= slot.AddHours(4))
                            {
                                swapPossibilities.Add(nowaWizytaPoczatek);
                                nowaWizytaPoczatek = nowaWizytaKoniec;
                                nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                            }
                        }
                        else
                        {
                            DateTime nowaWizytaPoczatek = wizytaKoniec;
                            DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                            DateTime nastepnaWizytaPoczatek = staffVisits[i + 1].Termin;
                            DateTime nastepnaWizytaKoniec = nastepnaWizytaPoczatek.AddHours(staffVisits[i + 1].CzasTrwania.Hours).AddMinutes(staffVisits[i + 1].CzasTrwania.Minutes);
                            while (nowaWizytaKoniec <= nastepnaWizytaPoczatek)
                            {
                                swapPossibilities.Add(nowaWizytaPoczatek);
                                nowaWizytaPoczatek = nowaWizytaKoniec;
                                nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                            }
                        }
                    }
                }
                if (swapPossibilities.Count > 0)
                {
                    var tempVisitList = visitList.ConvertAll(v => new Wizyta(v));
                    tempVisitList[randomVisitId].Termin = swapPossibilities[0];
                    tempVisitList[randomVisitId].Lekarz_Id = randomStaff.Id;
                    var bestMoveCost = CalculateCostFunction(tempVisitList);
                    var bestMovePerm = tempVisitList.ConvertAll(v => new Wizyta(v));
                    for (int i = 1; i < swapPossibilities.Count; i++)
                    {
                        tempVisitList = visitList.ConvertAll(v => new Wizyta(v));
                        tempVisitList[randomVisitId].Termin = swapPossibilities[i];
                        tempVisitList[randomVisitId].Lekarz_Id = randomStaff.Id;
                        var tempMin = CalculateCostFunction(tempVisitList);
                        if (tempMin <= bestMoveCost)
                        {
                            bestMoveCost = tempMin;
                            bestMovePerm = tempVisitList.ConvertAll(v => new Wizyta(v));
                        }
                    }
                    visitList = bestMovePerm.ConvertAll(v => new Wizyta(v));
                    visitSet = true;
                }
                randomStaffId++;
                if (randomStaffId % staffListCount == firstRandomStaffId % staffListCount)
                    break;
            }
            return visitList;
        }

        private static List<Wizyta> GenerateNewPermutationWithPacjentsCheck(List<Wizyta> visitList)
        {
            var staffList = Kadra.GetKadrasList();
            var staffListCount = staffList.Count;
            Random r = new Random();
            var randomVisitId = r.Next(0, visitList.Count);
            var randomStaffId = r.Next(0, staffListCount);
            bool visitSet = false;
            var firstRandomStaffId = randomStaffId;
            while (!visitSet)
            {
                var randomVisit = visitList[randomVisitId];
                var randomStaff = staffList[randomStaffId % staffListCount];
                var slot = GetSlotBeginning(randomVisit.Termin);
                var staffVisits = visitList.Where(v => v.Lekarz_Id == randomStaff.Id && v.Termin.Date == randomVisit.Termin.Date).OrderBy(v => v.Termin).ToList(); // tylko wizyty w tym samym dniu
                staffVisits = staffVisits.Where(v => v.Termin >= slot && v.Termin.AddHours(v.CzasTrwania.Hours).AddMinutes(v.CzasTrwania.Minutes) <= slot.AddHours(4)).ToList(); // tylko wizyty z danego przedzialu
                List<DateTime> swapPossibilities = new List<DateTime>();
                if (staffVisits.Count == 0)
                {
                    DateTime nowaWizytaPoczatek = slot;
                    DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                    while (nowaWizytaKoniec <= slot.AddHours(4))
                    {
                        if (Pacjent.CheckPacjentSchedule(visitList, randomVisit.Pacjent_Id, nowaWizytaPoczatek, randomVisit.CzasTrwania))
                            swapPossibilities.Add(nowaWizytaPoczatek);
                        nowaWizytaPoczatek = nowaWizytaKoniec;
                        nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                    }
                }
                else if (staffVisits.Count == 1)
                {
                    DateTime nowaWizytaPoczatek = slot;
                    DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                    DateTime wizytaPoczatek = staffVisits[0].Termin;
                    DateTime wizytaKoniec = wizytaPoczatek.AddHours(staffVisits[0].CzasTrwania.Hours).AddMinutes(staffVisits[0].CzasTrwania.Minutes);
                    while (nowaWizytaKoniec <= slot.AddHours(4))
                    {
                        if ((nowaWizytaPoczatek < wizytaPoczatek && nowaWizytaKoniec <= wizytaPoczatek) ||
                            (nowaWizytaPoczatek >= wizytaKoniec && nowaWizytaKoniec > wizytaKoniec))
                        {
                            if (Pacjent.CheckPacjentSchedule(visitList, randomVisit.Pacjent_Id, nowaWizytaPoczatek, randomVisit.CzasTrwania))
                                swapPossibilities.Add(nowaWizytaPoczatek);
                        }
                        nowaWizytaPoczatek = nowaWizytaKoniec;
                        nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                    }
                }
                else
                {
                    for (int i = 0; i < staffVisits.Count; i++)
                    {
                        DateTime wizytaPoczatek = staffVisits[i].Termin;
                        DateTime wizytaKoniec = wizytaPoczatek.AddHours(staffVisits[i].CzasTrwania.Hours).AddMinutes(staffVisits[i].CzasTrwania.Minutes);
                        if (i == 0) // jesli pierwsza wizyta z listy
                        {
                            DateTime nowaWizytaPoczatek = slot;
                            DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                            while (nowaWizytaKoniec <= wizytaPoczatek)
                            {
                                if (Pacjent.CheckPacjentSchedule(visitList, randomVisit.Pacjent_Id, nowaWizytaPoczatek, randomVisit.CzasTrwania))
                                    swapPossibilities.Add(nowaWizytaPoczatek);
                                nowaWizytaPoczatek = nowaWizytaKoniec;
                                nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                            }
                        }
                        else if (i == staffVisits.Count - 1) // jesli ostatnia wizyta z listy
                        {
                            DateTime nowaWizytaPoczatek = wizytaKoniec;
                            DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                            while (nowaWizytaKoniec <= slot.AddHours(4))
                            {
                                if (Pacjent.CheckPacjentSchedule(visitList, randomVisit.Pacjent_Id, nowaWizytaPoczatek, randomVisit.CzasTrwania))
                                    swapPossibilities.Add(nowaWizytaPoczatek);
                                nowaWizytaPoczatek = nowaWizytaKoniec;
                                nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                            }
                        }
                        else
                        {
                            DateTime nowaWizytaPoczatek = wizytaKoniec;
                            DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                            DateTime nastepnaWizytaPoczatek = staffVisits[i + 1].Termin;
                            DateTime nastepnaWizytaKoniec = nastepnaWizytaPoczatek.AddHours(staffVisits[i + 1].CzasTrwania.Hours).AddMinutes(staffVisits[i + 1].CzasTrwania.Minutes);
                            while (nowaWizytaKoniec <= nastepnaWizytaPoczatek)
                            {
                                if (Pacjent.CheckPacjentSchedule(visitList, randomVisit.Pacjent_Id, nowaWizytaPoczatek, randomVisit.CzasTrwania))
                                    swapPossibilities.Add(nowaWizytaPoczatek);
                                nowaWizytaPoczatek = nowaWizytaKoniec;
                                nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(randomVisit.CzasTrwania.Hours).AddMinutes(randomVisit.CzasTrwania.Minutes);
                            }
                        }
                    }
                }
                if (swapPossibilities.Count > 0)
                {
                    var tempVisitList = visitList.ConvertAll(v => new Wizyta(v));
                    tempVisitList[randomVisitId].Termin = swapPossibilities[0];
                    tempVisitList[randomVisitId].Lekarz_Id = randomStaff.Id;
                    var bestMoveCost = CalculateCostFunction(tempVisitList);
                    var bestMovePerm = tempVisitList.ConvertAll(v => new Wizyta(v));
                    for (int i = 1; i < swapPossibilities.Count; i++)
                    {
                        tempVisitList = visitList.ConvertAll(v => new Wizyta(v));
                        tempVisitList[randomVisitId].Termin = swapPossibilities[i];
                        tempVisitList[randomVisitId].Lekarz_Id = randomStaff.Id;
                        var tempMin = CalculateCostFunction(tempVisitList);
                        if (tempMin <= bestMoveCost)
                        {
                            bestMoveCost = tempMin;
                            bestMovePerm = tempVisitList.ConvertAll(v => new Wizyta(v));
                        }
                    }
                    visitList = bestMovePerm.ConvertAll(v => new Wizyta(v));
                    visitSet = true;
                }
                randomStaffId++;
                if (randomStaffId % staffListCount == firstRandomStaffId % staffListCount)
                    break;
            }
            return visitList;
        }

        private static DateTime GetSlotBeginning(DateTime visitTime)
        {

            if (visitTime.Hour >= 16)
                return visitTime.Date + new TimeSpan(16, 0, 0);
            else if (visitTime.Hour >= 12)
                return visitTime.Date + new TimeSpan(12, 0, 0);
            else
                return visitTime.Date + new TimeSpan(8, 0, 0);
        }
    }
}
