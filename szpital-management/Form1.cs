﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;

namespace szpital_management
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            UpdatePatientData();
            UpdateStaffData();
            UpdateStaffPositionData();
            UpdateHospitalData();
            UpdateVisitData();
        }

        private void ResetVisibility()
        {
            tabControlPatients.Visible = false;
            tabControlPatientsVisit.Visible = false;
            tabControlStaff.Visible = false;
            tabControlStaffVisit.Visible = false;
            tabControlHospitals.Visible = false;
            tabControlHospitalsVisit.Visible = false;
            tabControlVisits.Visible = false;
        }

        private void ButtonPatients_Click(object sender, EventArgs e)
        {
            ResetVisibility();
            tabControlPatients.Visible = true;
        }

        private void ButtonStaff_Click(object sender, EventArgs e)
        {
            ResetVisibility();
            tabControlStaff.Visible = true;
        }

        private void ButtonHospital_Click(object sender, EventArgs e)
        {
            ResetVisibility();
            tabControlHospitals.Visible = true;
        }

        private void ButtonVisits_Click(object sender, EventArgs e)
        {
            ResetVisibility();
            tabControlVisits.Visible = true;
        }

        private void UpdatePatientData()
        {
            dataGridPatient.Rows.Clear();
            var patientList = Pacjent.GetPacjentsList();
            foreach (Pacjent p in patientList)
            {
                string[] row = new string[] { p.Imie.Trim(), p.Nazwisko.Trim(), p.Pesel.Trim(), p.Telefon.Trim(), p.Miasto.Trim() };
                dataGridPatient.Rows.Add(row);
            }
            foreach (DataGridViewRow row in dataGridPatient.Rows)
            {
                row.HeaderCell.Value = (row.Index + 1).ToString();
            }
        }

        private void UpdateStaffData()
        {
            dataGridStaff.Rows.Clear();
            var staffList = Kadra.GetKadrasList();
            foreach (Kadra k in staffList)
            {
                var stanowisko = Stanowisko.FindStanowisko(k.Stanowisko_Id);
                string[] row = new string[] { k.Imie.Trim(), k.Nazwisko.Trim(), k.Pesel.Trim(), k.Telefon.Trim(), k.Miasto.Trim(), stanowisko.Zawód.Trim() + ", " + stanowisko.Specjalizacja.Trim() };
                dataGridStaff.Rows.Add(row);
            }
            foreach (DataGridViewRow row in dataGridStaff.Rows)
            {
                row.HeaderCell.Value = (row.Index + 1).ToString();
            }
        }

        private void UpdateStaffPositionData()
        {
            comboStaffAddPosition.Items.Clear();
            var positionList = Stanowisko.GetStanowiskoesList();
            List<string> staffCombo = new List<string>();
            foreach (Stanowisko s in positionList)
            {
                staffCombo.Add(s.Zawód.Trim() + ", " + s.Specjalizacja.Trim());
            }
            comboStaffAddPosition.Items.AddRange(staffCombo.ToArray());
        }

        private void UpdateHospitalData()
        {
            dataGridHospital.Rows.Clear();
            comboHospitalsRoomAddHospitalId.Items.Clear();
            comboVisitAddHospital.Items.Clear();
            var hospitalList = Szpital.GetSzpitalsList();
            List<string> hospitalCombo = new List<string>();
            foreach (Szpital s in hospitalList)
            {
                string[] row = new string[] { s.Nazwa.Trim(), s.Miasto.Trim(), s.Adres.Trim() };
                dataGridHospital.Rows.Add(row);
                hospitalCombo.Add(s.Nazwa.Trim());
            }
            foreach (DataGridViewRow row in dataGridHospital.Rows)
            {
                row.HeaderCell.Value = (row.Index + 1).ToString();
            }
            comboHospitalsRoomAddHospitalId.Items.AddRange(hospitalCombo.ToArray());
            comboVisitAddHospital.Items.AddRange(hospitalCombo.ToArray());
        }
        
        private void UpdateVisitData()
        {
            dataGridVisit.Rows.Clear();
            var visitList = Wizyta.GetWizytasList();
            foreach (Wizyta w in visitList)
            {
                var kadra = w.Lekarz_Id.HasValue ? Kadra.FindKadra(w.Lekarz_Id.Value) : null;
                var sala = Sala.FindSala(w.Sala_Id);
                var szpital = Szpital.FindSzpital(sala.Szpital_Id);
                var pacjent = Pacjent.FindPacjent(w.Pacjent_Id);
                string[] row = new string[] { w.Termin.ToString(), w.CzasTrwania.ToString(), kadra?.Imie.Trim() + " " + kadra?.Nazwisko.Trim(),
                    szpital.Nazwa.Trim() + ", " +  sala.Numer.ToString(), pacjent.Imie.Trim() + " " + pacjent.Nazwisko.Trim(), w.TypWizyty.Trim() };
                dataGridVisit.Rows.Add(row);
            }
            foreach (DataGridViewRow row in dataGridVisit.Rows)
            {
                row.HeaderCell.Value = (row.Index + 1).ToString();
            }
        }

        private void UpdatePatientVisitData(int id)
        {
            dataGridPatientVisit.Rows.Clear();
            var visitList = Wizyta.GetWizytasList();
            foreach (Wizyta w in visitList)
            {
                if (w.Pacjent_Id == id)
                {
                    var kadra = w.Lekarz_Id.HasValue ? Kadra.FindKadra(w.Lekarz_Id.Value) : null;
                    var sala = Sala.FindSala(w.Sala_Id);
                    var szpital = Szpital.FindSzpital(sala.Szpital_Id);
                    string[] row = new string[] { w.Termin.ToString(), w.CzasTrwania.ToString(), kadra?.Imie.Trim() + " " + kadra?.Nazwisko.Trim(),
                    szpital.Nazwa.Trim() + ", " +  sala.Numer.ToString(), w.TypWizyty.Trim() };
                    dataGridPatientVisit.Rows.Add(row);
                }
            }
            foreach (DataGridViewRow row in dataGridPatientVisit.Rows)
            {
                row.HeaderCell.Value = (row.Index + 1).ToString();
            }
        }

        private void UpdateStaffVisitData(int id)
        {
            dataGridStaffVisit.Rows.Clear();
            var visitList = Wizyta.GetWizytasList();
            foreach (Wizyta w in visitList)
            {
                if (w.Lekarz_Id.HasValue && w.Lekarz_Id.Value == id)
                {
                    var sala = Sala.FindSala(w.Sala_Id);
                    var szpital = Szpital.FindSzpital(sala.Szpital_Id);
                    var pacjent = Pacjent.FindPacjent(w.Pacjent_Id);
                    string[] row = new string[] { w.Termin.ToString(), w.CzasTrwania.ToString(), szpital.Nazwa.Trim() + ", " +  sala.Numer.ToString(),
                    pacjent.Imie.Trim() + " " + pacjent.Nazwisko.Trim(), w.TypWizyty.Trim() };
                    dataGridStaffVisit.Rows.Add(row);
                }
            }
            foreach (DataGridViewRow row in dataGridStaffVisit.Rows)
            {
                row.HeaderCell.Value = (row.Index + 1).ToString();
            }
        }

        private void UpdateHospitalVisitData(int id)
        {
            dataGridHospitalVisit.Rows.Clear();
            var visitList = Wizyta.GetWizytasList();
            foreach (Wizyta w in visitList)
            {
                var sala = Sala.FindSala(w.Sala_Id);
                if (sala.Szpital_Id == id)
                {
                    var kadra = w.Lekarz_Id.HasValue ? Kadra.FindKadra(w.Lekarz_Id.Value) : null;
                    var pacjent = Pacjent.FindPacjent(w.Pacjent_Id);
                    string[] row = new string[] { w.Termin.ToString(), w.CzasTrwania.ToString(), kadra?.Imie.Trim() + " " + kadra?.Nazwisko.Trim(),
                    pacjent.Imie.Trim() + " " +  pacjent.Nazwisko.Trim(), w.TypWizyty.Trim() };
                    dataGridHospitalVisit.Rows.Add(row);
                }
            }
            foreach (DataGridViewRow row in dataGridHospitalVisit.Rows)
            {
                row.HeaderCell.Value = (row.Index + 1).ToString();
            }
        }

        private void ButtonPatientAdd_Click(object sender, EventArgs e)
        {
            if (textPatientAddName.Text != "" && textPatientAddSurname.Text != "" && textPatientAddPesel.Text != "" && 
                textPatientAddPhone.Text != "" && textPatientAddCity.Text != "")
            {
                if (Walidacja.ValidatePhone(textPatientAddPhone.Text) && Walidacja.ValidatePesel(textPatientAddPesel.Text))
                {
                    Pacjent pacjent = new Pacjent(textPatientAddName.Text, textPatientAddSurname.Text, textPatientAddPesel.Text,
                        textPatientAddPhone.Text, textPatientAddCity.Text);
                    Pacjent.AddPacjent(pacjent);
                    MessageBox.Show("Pacjent został dodany!", "Sukces");
                    textPatientAddName.Text = "";
                    textPatientAddSurname.Text = "";
                    textPatientAddPesel.Text = "";
                    textPatientAddPhone.Text = "";
                    textPatientAddCity.Text = "";
                    UpdatePatientData();
                }
                else
                {
                    MessageBox.Show("Podaj poprawny numer telefonu i PESEL!", "Błąd");
                }
            }
            else
            {
                MessageBox.Show("Uzupełnij wszystkie pola!", "Błąd");
            }
        }

        private void ButtonPatientChoose_Click(object sender, EventArgs e)
        {
            var patientList = Pacjent.GetPacjentsList();
            if (dataGridPatient.CurrentCell.RowIndex >= 0 && dataGridPatient.CurrentCell.RowIndex < patientList.Count)
            {
                Pacjent pacjent = Pacjent.FindPacjent(patientList[dataGridPatient.CurrentCell.RowIndex].Id);
                UpdatePatientVisitData(pacjent.Id);
                ResetVisibility();
                tabControlPatientsVisit.Visible = true;
                labelPatientVisitTitle.Text = pacjent.Imie.Trim() + " " + pacjent.Nazwisko.Trim() + " - wizyty";
            }
        }

        private void ButtonPatientDelete_Click(object sender, EventArgs e)
        {
            var patientList = Pacjent.GetPacjentsList();
            if (dataGridPatient.CurrentCell.RowIndex >= 0 && dataGridPatient.CurrentCell.RowIndex < patientList.Count)
            {
                Pacjent.DeletePacjent(patientList[dataGridPatient.CurrentCell.RowIndex].Id);
                MessageBox.Show("Pacjent został usunięty!", "Sukces");
                UpdatePatientData();
            }
        }
        private void ButtonPatientVisitBook_Click(object sender, EventArgs e)
        {
            panelSideNav.Enabled = false;
            tabControlPatientsVisit.Enabled = false;
            panelVisitAdd.Visible = true;
            panelVisitAdd.BringToFront();
            var patientList = Pacjent.GetPacjentsList();
            var patient = Pacjent.FindPacjent(patientList[dataGridPatient.CurrentCell.RowIndex].Id);
            labelVisitAddTitle.Text = "Wizyta - " + patient.Imie.Trim() + " " + patient.Nazwisko.Trim();
        }

        private void ButtonStaffAdd_Click(object sender, EventArgs e)
        {
            if (textStaffAddName.Text != "" && textStaffAddSurname.Text != "" && textStaffAddPesel.Text != "" &&
                textStaffAddPhone.Text != "" && textStaffAddCity.Text != "" && comboStaffAddPosition.SelectedItem != null)
            {
                if (Walidacja.ValidatePhone(textStaffAddPhone.Text) && Walidacja.ValidatePesel(textStaffAddPesel.Text))
                {
                    var zawód = comboStaffAddPosition.SelectedItem.ToString().Split(',')[0];
                    var specjalizacja = comboStaffAddPosition.SelectedItem.ToString().Split(',')[1].Trim();
                    var stanowisko = Stanowisko.FindStanowiskoByName(zawód, specjalizacja);
                    Kadra kadra = new Kadra(textStaffAddName.Text, textStaffAddSurname.Text, textStaffAddPesel.Text,
                        textStaffAddPhone.Text, textStaffAddCity.Text, stanowisko.Id);
                    Kadra.AddKadra(kadra);
                    panelSideNav.Enabled = false;
                    tabControlStaff.Enabled = false;
                    panelScheduleAdd.Visible = true;
                    panelScheduleAdd.BringToFront();
                    labelScheduleStaffName.Text = "Harmonogram - " + textStaffAddName.Text.Trim() + " " + textStaffAddSurname.Text.Trim();
                    textStaffAddName.Text = "";
                    textStaffAddSurname.Text = "";
                    textStaffAddPesel.Text = "";
                    textStaffAddPhone.Text = "";
                    textStaffAddCity.Text = "";
                    comboStaffAddPosition.SelectedItem = null;
                    UpdateStaffData();
                }
                else
                {
                    MessageBox.Show("Podaj poprawny numer telefonu i PESEL!", "Błąd");
                }
            }
            else
            {
                MessageBox.Show("Uzupełnij wszystkie pola!", "Błąd");
            }
        }

        private void ButtonStaffChoose_Click(object sender, EventArgs e)
        {
            var staffList = Kadra.GetKadrasList();
            if (dataGridStaff.CurrentCell.RowIndex >= 0 && dataGridStaff.CurrentCell.RowIndex < staffList.Count)
            {
                Kadra kadra = Kadra.FindKadra(staffList[dataGridStaff.CurrentCell.RowIndex].Id);
                UpdateStaffVisitData(kadra.Id);
                ResetVisibility();
                tabControlStaffVisit.Visible = true;
                labelStaffVisitTitle.Text = kadra.Imie.Trim() + " " + kadra.Nazwisko.Trim() + " - wizyty";
            }
        }

        private void ButtonStaffDelete_Click(object sender, EventArgs e)
        {
            var staffList = Kadra.GetKadrasList();
            if (dataGridStaff.CurrentCell.RowIndex >= 0 && dataGridStaff.CurrentCell.RowIndex < staffList.Count)
            {
                Kadra.DeleteKadra(staffList[dataGridStaff.CurrentCell.RowIndex].Id);
                MessageBox.Show("Członek personelu został usunięty!", "Sukces");
                UpdateStaffData();
            }
        }

        private void ButtonStaffPositionAdd_Click(object sender, EventArgs e)
        {
            if (comboStaffPositionAddJob.SelectedItem != null && textStaffPositionAddSpecialization.Text != "")
            {
                Stanowisko stanowisko = new Stanowisko(comboStaffPositionAddJob.SelectedItem.ToString(), textStaffPositionAddSpecialization.Text);
                Stanowisko.AddStanowisko(stanowisko);
                MessageBox.Show("Stanowisko zostało dodane!", "Sukces");
                comboStaffPositionAddJob.SelectedItem = null;
                textStaffPositionAddSpecialization.Text = "";
                UpdateStaffPositionData();
            }
            else
            {
                MessageBox.Show("Uzupełnij wszystkie pola!", "Błąd");
            }
        }

        private void ButtonHospitalAdd_Click(object sender, EventArgs e)
        {
            if (textHospitalAddName.Text != "" && textHospitalAddCity.Text != "" && textHospitalAddAddress.Text != "")
            {
                Szpital szpital = new Szpital(textHospitalAddName.Text, textHospitalAddCity.Text, textHospitalAddAddress.Text);
                Szpital.AddSzpital(szpital);
                MessageBox.Show("Szpital został dodany!", "Sukces");
                textHospitalAddName.Text = "";
                textHospitalAddCity.Text = "";
                textHospitalAddAddress.Text = "";
                UpdateHospitalData();
            }
            else
            {
                MessageBox.Show("Uzupełnij wszystkie pola!", "Błąd");
            }
        }

        private void ButtonHospitalChoose_Click(object sender, EventArgs e)
        {
            var hospitalList = Szpital.GetSzpitalsList();
            if (dataGridHospital.CurrentCell.RowIndex >= 0 && dataGridHospital.CurrentCell.RowIndex < hospitalList.Count)
            {
                Szpital szpital = Szpital.FindSzpital(hospitalList[dataGridHospital.CurrentCell.RowIndex].Id);
                UpdateHospitalVisitData(szpital.Id);
                ResetVisibility();
                tabControlHospitalsVisit.Visible = true;
                labelHospitalVisitTitle.Text = szpital.Nazwa.Trim() + " - wizyty";
            }
        }

        private void ButtonHospitalDelete_Click(object sender, EventArgs e)
        {
            var hospitalList = Szpital.GetSzpitalsList();
            if (dataGridHospital.CurrentCell.RowIndex >= 0 && dataGridHospital.CurrentCell.RowIndex < hospitalList.Count)
            {
                Szpital.DeleteSzpital(hospitalList[dataGridHospital.CurrentCell.RowIndex].Id);
                MessageBox.Show("Szpital został usunięty!", "Sukces");
                UpdateHospitalData();
            }
        }

        private void ButtonHospitalsRoomAdd_Click(object sender, EventArgs e)
        {
            if (textHospitalsRoomAddNumber.Text != "" && textHospitalsRoomAddFloor.Text != "" && comboHospitalsRoomAddHospitalId.SelectedItem != null)
            {
                var nazwaSzpitala = comboHospitalsRoomAddHospitalId.SelectedItem.ToString();
                var szpital = Szpital.FindSzpitalByName(nazwaSzpitala);
                Sala sala = new Sala(textHospitalsRoomAddNumber.Text, textHospitalsRoomAddFloor.Text, szpital.Id);
                Sala.AddSala(sala);
                textHospitalsRoomAddNumber.Text = "";
                textHospitalsRoomAddFloor.Text = "";
                comboHospitalsRoomAddHospitalId.SelectedItem = null;
                MessageBox.Show("Sala została dodana!", "Sukces");
            }
            else
            {
                MessageBox.Show("Uzupełnij wszystkie pola!", "Błąd");
            } 
        }

        private void ButtonScheduleSave_Click(object sender, EventArgs e)
        {
            HarmonogramCzasuPracy harmonogram = new HarmonogramCzasuPracy();
            if(checkMonday.Checked && comboMondayFrom.SelectedItem != null && comboMondayTo.SelectedItem != null)
            {
                harmonogram.PoniedzialekStart = TimeSpan.Parse(comboMondayFrom.SelectedItem.ToString());
                harmonogram.PoniedzialekKoniec = TimeSpan.Parse(comboMondayTo.SelectedItem.ToString());
            }
            if (checkTuesday.Checked && comboTuesdayTo.SelectedItem != null && comboTuesdayTo.SelectedItem != null)
            {
                harmonogram.WtorekStart = TimeSpan.Parse(comboTuesdayFrom.SelectedItem.ToString());
                harmonogram.WtorekKoniec = TimeSpan.Parse(comboTuesdayTo.SelectedItem.ToString());
            }
            if (checkWednesday.Checked && comboWednesdayTo.SelectedItem != null && comboWednesdayTo.SelectedItem != null)
            {
                harmonogram.SrodaStart = TimeSpan.Parse(comboWednesdayFrom.SelectedItem.ToString());
                harmonogram.SrodaKoniec = TimeSpan.Parse(comboWednesdayTo.SelectedItem.ToString());
            }
            if (checkThursday.Checked && comboThursdayTo.SelectedItem != null && comboThursdayTo.SelectedItem != null)
            {
                harmonogram.CzwartekStart = TimeSpan.Parse(comboThursdayFrom.SelectedItem.ToString());
                harmonogram.CzwartekKoniec = TimeSpan.Parse(comboThursdayTo.SelectedItem.ToString());
            }
            if (checkFriday.Checked && comboFridayTo.SelectedItem != null && comboFridayTo.SelectedItem != null)
            {
                harmonogram.PiatekStart = TimeSpan.Parse(comboFridayFrom.SelectedItem.ToString());
                harmonogram.PiatekKoniec = TimeSpan.Parse(comboFridayTo.SelectedItem.ToString());
            }
            var harmonogramId = HarmonogramCzasuPracy.AddHarmonogram(harmonogram);
            var kadra = Kadra.FindKadraByName(labelScheduleStaffName.Text.Split(' ')[2].Trim(), labelScheduleStaffName.Text.Split(' ')[3].Trim());
            kadra.Harmonogram_Id = harmonogramId;
            Kadra.UpdateKadra(kadra);
            MessageBox.Show("Członek kadry został dodany!", "Sukces");
            panelSideNav.Enabled = true;
            tabControlStaff.Enabled = true;
            panelScheduleAdd.Visible = false;
            panelScheduleAdd.SendToBack();
            labelScheduleStaffName.Text = "";
        }

        private void ButtonVisitSave_Click(object sender, EventArgs e)
        {
            if (pickerVisitAddDate.Text != null && comboVisitAddHours.SelectedItem != null &&
                comboVisitAddHospital.SelectedItem != null && comboVisitAddType.SelectedItem != null)
            {
                string dateString = pickerVisitAddDate.Text + " " + comboVisitAddHours.SelectedItem.ToString().Substring(0, 5);
                DateTime date = DateTime.ParseExact(dateString, "dd.MM.yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                TimeSpan length = GetVisitLength(comboVisitAddType.SelectedItem.ToString());
                int roomId = GetHospitalRoom(comboVisitAddHospital.SelectedItem.ToString(), date, length);
                if (roomId == -1)
                    MessageBox.Show("Nie ma dostępnych terminów wizyt w wybranym przedziale czasu!", "Błąd");
                else
                {
                    var patientList = Pacjent.GetPacjentsList();
                    var patient = Pacjent.FindPacjent(patientList[dataGridPatient.CurrentCell.RowIndex].Id);
                    Wizyta wizyta = new Wizyta(date, length, roomId, patient.Id, comboVisitAddType.SelectedItem.ToString(), false);
                    Wizyta.AddWizyta(wizyta);
                    UpdateVisitData();
                    UpdatePatientVisitData(patient.Id);
                    MessageBox.Show("Wizyta została zarejestrowana!", "Sukces");
                }
                panelSideNav.Enabled = true;
                tabControlPatientsVisit.Enabled = true;
                panelVisitAdd.Visible = false;
                panelVisitAdd.SendToBack();
                pickerVisitAddDate.Text = null;
                comboVisitAddHours.SelectedItem = null;
                comboVisitAddHospital.SelectedItem = null;
                comboVisitAddType.SelectedItem = null;
            }
            else
            {
                MessageBox.Show("Uzupełnij wszystkie pola!", "Błąd");
            }
        }

        private void ButtonVisitAssign_Click(object sender, EventArgs e)
        {
            SA.InitialSetup();
            SA.SimulatedAnnealing();
            UpdateVisitData();
            DisplaySchedule();
        }

        private void ButtonVisitDelete_Click(object sender, EventArgs e)
        {
            var visitList = Wizyta.GetWizytasList();
            if (dataGridVisit.CurrentCell.RowIndex >= 0 && dataGridVisit.CurrentCell.RowIndex < visitList.Count)
            {
                Wizyta.DeleteWizyta(visitList[dataGridVisit.CurrentCell.RowIndex].Id);
                MessageBox.Show("Wizyta została usunięta!", "Sukces");
                UpdateVisitData();
            }
        }

        private void CheckMonday_Click(object sender, EventArgs e)
        {
            comboMondayFrom.Enabled = !comboMondayFrom.Enabled;
            comboMondayTo.Enabled = !comboMondayTo.Enabled;
        }
        private void CheckTuesday_Click(object sender, EventArgs e)
        {
            comboTuesdayFrom.Enabled = !comboTuesdayFrom.Enabled;
            comboTuesdayTo.Enabled = !comboTuesdayTo.Enabled;
        }
        private void CheckWednesday_Click(object sender, EventArgs e)
        {
            comboWednesdayFrom.Enabled = !comboWednesdayFrom.Enabled;
            comboWednesdayTo.Enabled = !comboWednesdayTo.Enabled;
        }
        private void CheckThursday_Click(object sender, EventArgs e)
        {
            comboThursdayFrom.Enabled = !comboThursdayFrom.Enabled;
            comboThursdayTo.Enabled = !comboThursdayTo.Enabled;
        }
        private void CheckFriday_Click(object sender, EventArgs e)
        {
            comboFridayFrom.Enabled = !comboFridayFrom.Enabled;
            comboFridayTo.Enabled = !comboFridayTo.Enabled;
        }

        private TimeSpan GetVisitLength(string visitType)
        {
            switch (visitType)
            {
                case "Konsultacja":
                    return TimeSpan.Parse("00:15");
                case "Zabieg":
                    return TimeSpan.Parse("00:30");
                case "Operacja":
                    return TimeSpan.Parse("01:00");
                case "Badanie":
                    return TimeSpan.Parse("00:30");
                default:
                    return TimeSpan.Parse("00:30");
            }
        }

        private int GetHospitalRoom(string nazwaSzpitala, DateTime dataWizyty, TimeSpan dlugoscWizyty)
        {
            var hospital = Szpital.FindSzpitalByName(nazwaSzpitala);
            var roomList = Sala.GetSalasList();
            var visitList = Wizyta.GetWizytasList();
            roomList = roomList.Where(x => x.Szpital_Id == hospital.Id).ToList();
            if (roomList.Count != 0) {
                if (visitList.Count == 0)
                {
                    return roomList[0].Id;
                }
                foreach (Sala r in roomList)
                {
                    List<Wizyta> rezerwacjaSali = new List<Wizyta>();
                    for (int i = 0; i < visitList.Count; i++)
                    {
                        if (r.Id == visitList[i].Sala_Id && dataWizyty.Date == visitList[i].Termin.Date)
                        {
                            DateTime wizytaPoczatek = visitList[i].Termin;
                            DateTime wizytaKoniec = visitList[i].Termin.AddHours(visitList[i].CzasTrwania.Hours).AddMinutes(visitList[i].CzasTrwania.Minutes);
                            DateTime poczatekPrzedzialu = dataWizyty;
                            DateTime koniecPrzedzialu = dataWizyty.AddHours(4);
                            if (poczatekPrzedzialu <= wizytaPoczatek && koniecPrzedzialu >= wizytaKoniec)
                                rezerwacjaSali.Add(visitList[i]);
                        } 
                    }
                    if (rezerwacjaSali.Count == 0)
                        return r.Id;
                    DateTime nowaWizytaPoczatek = dataWizyty;
                    DateTime nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(dlugoscWizyty.Hours).AddMinutes(dlugoscWizyty.Minutes);
                    while (nowaWizytaKoniec <= dataWizyty.AddHours(4))
                    {
                        bool terminZajety = false;
                        foreach (Wizyta w in rezerwacjaSali)
                        {
                            DateTime wizytaPoczatek = w.Termin;
                            DateTime wizytaKoniec = w.Termin.AddHours(w.CzasTrwania.Hours).AddMinutes(w.CzasTrwania.Minutes);
                            if (!((nowaWizytaPoczatek < wizytaPoczatek && nowaWizytaKoniec <= wizytaPoczatek) || 
                                (nowaWizytaPoczatek >= wizytaKoniec && nowaWizytaKoniec > wizytaKoniec)))
                            {
                                terminZajety = true;
                                break;
                            }
                        }
                        if (terminZajety == false)
                            return r.Id;
                        nowaWizytaPoczatek = nowaWizytaKoniec;
                        nowaWizytaKoniec = nowaWizytaPoczatek.AddHours(dlugoscWizyty.Hours).AddMinutes(dlugoscWizyty.Minutes);
                    }
                }
            }
            return -1;
        }

        private void DisplaySchedule()
        {
            var visitList = Wizyta.GetWizytasList();
            var date = visitList[0].Termin.Date;
            var lastDate = visitList[visitList.Count - 1].Termin.Date;
            for (; date <= lastDate; date = date.AddDays(1))
            {
                if (visitList.Where(v => v.Termin.Date == date).ToList().Count > 0)
                {
                    Console.WriteLine();
                    DisplaySchedulesDay(date);
                }
            }
        }

        private void DisplaySchedulesDay(DateTime date)
        {
            Console.WriteLine(date.ToShortDateString());
            for (int k = 0; k < 12; k++)
            {
                var hour = k  + 8;
                if (hour < 10)
                    Console.Write(hour + "    ");
                else
                    Console.Write(hour + "   ");
            }
            Console.WriteLine();
            var staffList = Kadra.GetKadrasList();
            foreach (Kadra k in staffList)
            {
                string[] schedule = new string[48];
                for (int i = 0; i < schedule.Length; i++)
                    schedule[i] = "o";
                var staffVisitList = Wizyta.GetWizytasByDoctorList(k.Id).Where(v => v.Termin.Date == date);
                foreach (Wizyta w in staffVisitList)
                {
                    var it = (w.Termin.Hour - 8) * 4 + (w.Termin.Minute / 15);
                    for (int j = it; j < it + (w.CzasTrwania.Hours * 4 + w.CzasTrwania.Minutes / 15); j++)
                    {
                        if (j == it)
                            schedule[j] = "<"; // poczatek wizyty
                        else if (j == it + (w.CzasTrwania.Hours * 4 + w.CzasTrwania.Minutes / 15) - 1)
                            schedule[j] = ">"; // koniec wizyty
                        else
                            schedule[j] = "="; // srodek wizyty
                    }
                }
                Console.WriteLine(k.Imie.Trim() + " " + k.Nazwisko.Trim());
                for (int i = 0; i < schedule.Length; i++)
                {
                    if (i % 4 == 0)
                        Console.Write("|");
                    Console.Write(schedule[i]);
                }
                Console.WriteLine();
            }
        }
    }
}
