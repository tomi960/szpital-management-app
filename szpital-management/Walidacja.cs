﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace szpital_management
{
    class Walidacja
    {
        public static bool ValidatePhone(string phone)
        {
            if (phone.Length == 9)
            {
                return true;
            }
            return false;
        }

        public static bool ValidatePesel(string pesel)
        {
            if (pesel.Length == 11)
            {
                var suma = CountPeselSum(pesel);
                if (suma == pesel[10].ToString())
                    return true;
            }
            return false;
        }

        private static string CountPeselSum(string pesel)
        {
            int[] mnozniki = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };
            int sum = 0;
            for (int i = 0; i < mnozniki.Length; i++)
            {
                sum += mnozniki[i] * int.Parse(pesel[i].ToString());
            }

            int reszta = sum % 10;
            return reszta == 0 ? reszta.ToString() : (10 - reszta).ToString();
        }
    }
}
